/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.seller.promotion;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.promotion.minus.dos.MinusDO;
import cn.shoptnt.model.promotion.minus.vo.MinusVO;
import cn.shoptnt.service.promotion.minus.MinusManager;
import cn.shoptnt.framework.context.user.UserContext;
import cn.shoptnt.model.util.PromotionValid;
import cn.shoptnt.framework.exception.NoPermissionException;
import cn.shoptnt.framework.security.model.Seller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;

/**
 * 单品立减促销活动控制器
 *
 * @author Snow
 * @version v7.0.0
 * @since v7.0.0
 * 2018-03-23 19:52:27
 */
@SuppressWarnings("Duplicates")
@RestController
@RequestMapping("/seller/promotion/minus")
@Tag(name = "单品立减促销活动相关API")
@Validated
public class MinusSellerController {

    @Autowired
    private MinusManager minusManager;

    @Operation(summary = "查询单品立减促销活动分页数据列表")
    @Parameters({
            @Parameter(name = "page_no", description = "页码", in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量", in = ParameterIn.QUERY),
            @Parameter(name = "keywords", description = "查询关键字", in = ParameterIn.QUERY)
    })
    @GetMapping
    public WebPage<MinusVO> list(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize, String keywords) {
        return this.minusManager.list(pageNo, pageSize, keywords);
    }


    @Operation(summary = "添加单品立减促销活动信息")
    @Parameter(name = "minus", description = "单品立减促销活动信息", required = true)
    @PostMapping
    public MinusVO add(@Parameter(hidden = true) @Valid @RequestBody MinusVO minus) {
        //参数验证 活动时间和参与活动商品的验证
        PromotionValid.paramValid(minus.getStartTime(), minus.getEndTime(), minus.getRangeType(), minus.getGoodsList());
        // 获取当前登录的商家信息
        Seller seller = UserContext.getSeller();
        //获取商家ID
        Long sellerId = seller.getSellerId();
        //设置商家ID
        minus.setSellerId(sellerId);
        //添加单品立减促销活动信息
        this.minusManager.add(minus);
        return minus;

    }

    @PutMapping(value = "/{id}")
    @Operation(summary = "修改单品立减促销活动信息")
    @Parameters({
            @Parameter(name = "id", description = "单品立减促销活动主键ID", required = true, in = ParameterIn.PATH)
    })
    public MinusVO edit(@Valid @RequestBody MinusVO minus, @PathVariable Long id) {
        //操作权限校验（如果当前修改的单品立减活动不属于当前操作的商家则不允许操作）
        this.minusManager.verifyAuth(id);
        //参数验证 活动时间和参与活动商品的验证
        PromotionValid.paramValid(minus.getStartTime(), minus.getEndTime(), minus.getRangeType(), minus.getGoodsList());
        //设置单品立减活动ID
        minus.setMinusId(id);
        //修改单品立减促销活动信息
        this.minusManager.edit(minus, id);
        return minus;
    }


    @DeleteMapping(value = "/{id}")
    @Operation(summary = "删除单品立减促销活动信息")
    @Parameters({
            @Parameter(name = "id", description = "要删除的单品立减促销活动信息主键ID", required = true, in = ParameterIn.PATH)
    })
    public String delete(@PathVariable Long id) {
        //操作权限校验（如果当前修改的单品立减活动不属于当前操作的商家则不允许操作）
        this.minusManager.verifyAuth(id);
        //删除单品立减促销活动信息
        this.minusManager.delete(id);
        return "";
    }


    @GetMapping(value = "/{id}")
    @Operation(summary = "查询一个单品立减促销活动信息")
    @Parameters({
            @Parameter(name = "id", description = "要查询的单品立减促销活动信息主键ID", required = true, in = ParameterIn.PATH)
    })
    public MinusVO get(@PathVariable Long id) {
        //根据主键ID获取单品立减促销活动信息
        MinusVO minusVO = this.minusManager.getFromDB(id);
        //获取当前登录的商家信息
        Seller seller = UserContext.getSeller();

        //验证越权操作
        if (minusVO == null || !seller.getSellerId().equals(minusVO.getSellerId())) {
            throw new NoPermissionException("无权操作");
        }

        return minusVO;
    }


}
