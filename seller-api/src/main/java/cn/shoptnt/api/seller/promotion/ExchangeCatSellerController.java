/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.seller.promotion;

import cn.shoptnt.service.promotion.exchange.ExchangeCatManager;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import java.util.List;

/**
 * 积分分类相关API
 *
 * @author Snow create in 2018/7/24
 * @version v2.0
 * @since v7.0.0
 */
@RestController
@RequestMapping("/seller/promotion/exchange-cats")
@Tag(name = "积分分类相关API")
@Validated
public class ExchangeCatSellerController {

    @Autowired
    private ExchangeCatManager exchangeCatManager;

    @Operation(summary = "查询某分类下的子分类列表")
    @Parameters({
            @Parameter(name = "parent_id", description = "父id，顶级为0", required = true, in = ParameterIn.PATH)
    })
    @GetMapping(value = "/{parent_id}/children")
    public List list(@Parameter(hidden = true) @PathVariable("parent_id") Long parentId) {
        return this.exchangeCatManager.list(parentId);
    }

}
