/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.seller.promotion;

import cn.shoptnt.model.promotion.coupon.dos.CouponDO;
import cn.shoptnt.model.promotion.coupon.dto.CouponParams;
import cn.shoptnt.service.promotion.coupon.CouponManager;
import cn.shoptnt.framework.context.user.UserContext;
import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.framework.exception.NoPermissionException;
import cn.shoptnt.framework.security.model.Seller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;
import java.util.List;

/**
 * 店铺优惠券控制器
 *
 * @author Snow
 * @version v2.0
 * @since v7.0.0
 * 2018-04-17 23:19:39
 */
@SuppressWarnings("Duplicates")
@RestController
@RequestMapping("/seller/promotion/coupons")
@Tag(name = "店铺优惠券相关API")
@Validated
public class CouponSellerController {

    @Autowired
    private CouponManager couponManager;

    @Operation(summary = "查询店铺优惠券分页数据列表")
    @GetMapping
    public WebPage list(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize, CouponParams params) {
        params.setPageNo(pageNo);
        params.setPageSize(pageSize);
        //获取当前登录的商家信息
        Seller seller = UserContext.getSeller();
        //设置商家ID作为查询条件
        params.setSellerId(seller.getSellerId());
        return this.couponManager.list(params);
    }

    @Operation(summary = "新增店铺优惠券信息")
    @PostMapping
    public CouponDO add(@Valid CouponDO couponDO) {
        //获取当前登录的商家信息
        Seller seller = UserContext.getSeller();
        //设置优惠券所属商家ID
        couponDO.setSellerId(seller.getSellerId());
        //设置优惠券所属商家名称
        couponDO.setSellerName(seller.getSellerName());
        //新增店铺优惠券信息
        this.couponManager.add(couponDO);
        return couponDO;
    }

    @PutMapping(value = "/{coupon_id}")
    @Operation(summary = "修改店铺优惠券信息")
    @Parameter(name = "coupon_id", description = "优惠券主键ID", required = true, in = ParameterIn.PATH)
    public CouponDO add(@Parameter(hidden = true) @PathVariable("coupon_id") Long couponId, @Valid CouponDO couponDO) {
        //设置优惠券主键ID
        couponDO.setCouponId(couponId);
        //获取当前登录的商家信息
        Seller seller = UserContext.getSeller();
        //设置优惠券所属商家ID
        couponDO.setSellerId(seller.getSellerId());
        //设置优惠券所属商家名称
        couponDO.setSellerName(seller.getSellerName());
        //修改店铺优惠券信息
        this.couponManager.edit(couponDO, couponId);
        return couponDO;
    }

    @DeleteMapping(value = "/{id}")
    @Operation(summary = "删除店铺优惠券信息")
    @Parameters({
            @Parameter(name = "id", description = "要删除的优惠券主键ID", required = true, in = ParameterIn.PATH)
    })
    public String delete(@PathVariable Long id) {
        //操作权限校验（如果当前删除的优惠券不属于当前操作的商家则不允许操作）
        this.couponManager.verifyAuth(id);
        //删除优惠券信息
        this.couponManager.delete(id);
        return "";
    }

    @GetMapping(value = "/{id}")
    @Operation(summary = "查询一个店铺优惠券信息")
    @Parameters({
            @Parameter(name = "id", description = "要查询的优惠券主键ID", required = true, in = ParameterIn.PATH)
    })
    public CouponDO get(@PathVariable Long id) {
        //根据ID获取店铺优惠券信息
        CouponDO coupon = this.couponManager.getModel(id);
        //操作权限校验（如果当前删除的优惠券为空或者不属于当前操作的商家则不允许操作）
        if (coupon == null || !coupon.getSellerId().equals(UserContext.getSeller().getSellerId())) {
            throw new NoPermissionException("无权操作或者数据不存在");
        }
        return coupon;
    }

    @GetMapping(value = "/{status}/list")
    @Operation(summary = "根据优惠券状态获取优惠券数据集合")
    @Parameters({
            @Parameter(name = "status", description = "优惠券状态 0：全部，1：有效，2：失效", required = true, in = ParameterIn.PATH, example = "0：全部，1：有效，2：失效")
    })
    public List<CouponDO> getByStatus(@PathVariable Integer status) {
        return this.couponManager.getByStatus(UserContext.getSeller().getSellerId(), status);
    }

}

