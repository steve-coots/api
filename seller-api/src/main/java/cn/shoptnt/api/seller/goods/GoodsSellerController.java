/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.seller.goods;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.goods.dos.GoodsDO;
import cn.shoptnt.model.goods.dto.GoodsDTO;
import cn.shoptnt.model.goods.dto.GoodsQueryParam;
import cn.shoptnt.model.goods.enums.Permission;
import cn.shoptnt.model.goods.vo.GoodsSelectLine;
import cn.shoptnt.model.goods.vo.GoodsVO;
import cn.shoptnt.model.support.LogClient;
import cn.shoptnt.model.support.validator.annotation.Log;
import cn.shoptnt.service.goods.GoodsManager;
import cn.shoptnt.service.goods.GoodsQueryManager;
import cn.shoptnt.framework.context.user.UserContext;
import cn.shoptnt.framework.security.model.Seller;
import cn.shoptnt.framework.util.StringUtil;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;
import java.util.List;

/**
 * 商品控制器
 *
 * @author fk
 * @version v2.0
 * @since v7.0.0 2018-03-21 11:23:10
 */
@RestController
@RequestMapping("/seller/goods")
@Tag(name = "商品相关API")
@Validated
@Scope("request")
public class GoodsSellerController {

	@Autowired
	private GoodsQueryManager goodsQueryManager;

	@Autowired
	private GoodsManager goodsManager;

	@Operation(summary = "查询商品列表")
	@GetMapping
	public WebPage list(GoodsQueryParam param, @Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize) {

		param.setPageNo(pageNo);
		param.setPageSize(pageSize);

		Seller seller = UserContext.getSeller();
		param.setSellerId(seller.getSellerId());
		return this.goodsQueryManager.list(param);
	}

	@Operation(summary = "查询预警商品列表")
	@GetMapping("/warning")
	public WebPage warningList(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize, GoodsQueryParam param){

		param.setPageNo(pageNo);
		param.setPageSize(pageSize);

		Seller seller = UserContext.getSeller();
		param.setSellerId(seller.getSellerId());
		return this.goodsQueryManager.warningGoodsList(param);

	}


	@Operation(summary = "添加商品")
	@Parameter(name = "goods", description = "商品信息", required = true)
	@PostMapping
	@Log(client = LogClient.seller,detail = "添加商品，商品名称：${goods.goodsName}")
	public GoodsDO add(@Parameter(hidden = true) @Valid @RequestBody GoodsDTO goods) {
		GoodsDO  goodsDO = this.goodsManager.add(goods);
		return goodsDO;
	}

	@PutMapping(value = "/{id}")
	@Operation(summary = "修改商品")
	@Parameters({
		@Parameter(name = "id", description = "主键", required = true,  in = ParameterIn.PATH),
		@Parameter(name = "goods", description = "商品信息", required = true)
	})
	@Log(client = LogClient.seller,detail = "编辑商品，商品id：${id}")
	public GoodsDO edit(@Valid @RequestBody GoodsDTO goods, @PathVariable Long id) {
		GoodsDO goodsDO = this.goodsManager.edit(goods, id);
		return goodsDO;
	}

	@GetMapping(value = "/{id}")
	@Operation(summary = "查询一个商品,商家编辑时使用")
	@Parameters({
			@Parameter(name = "id", description = "要查询的商品主键", required = true,  in = ParameterIn.PATH) })
	public GoodsVO get(@PathVariable Long id) {

		GoodsVO goods = this.goodsQueryManager.sellerQueryGoods(id);

		return goods;
	}

	@Operation(summary = "商家下架商品")
	@Parameters({
		@Parameter(name="goods_ids",description="商品ID集合",required=true)
	})
	@PutMapping(value = "/{goods_ids}/under")
	public String underGoods(@PathVariable("goods_ids") Long[] goodsIds,String reason){
		if(StringUtil.isEmpty(reason)){
			reason = "自行下架，无原因";
		}
		this.goodsManager.under(goodsIds,reason,Permission.SELLER);

		return null;
	}

	@Operation(summary = "商家将商品放入回收站")
	@Parameters({
		@Parameter(name="goods_ids",description="商品ID集合",required=true)
	})
	@PutMapping(value = "/{goods_ids}/recycle")
	public String deleteGoods(@PathVariable("goods_ids") Long[] goodsIds){

		this.goodsManager.inRecycle(goodsIds);

		return null;
	}

	@Operation(summary = "商家还原商品")
	@Parameters({
		@Parameter(name="goods_ids",description="商品ID集合",required=true)
	})
	@PutMapping(value="/{goods_ids}/revert")
	public String revertGoods(@PathVariable("goods_ids") Long[] goodsIds){

		this.goodsManager.revert(goodsIds);

		return null;
	}

	@Operation(summary = "商家彻底删除商品")
	@Parameters({
		@Parameter(name="goods_ids",description="商品ID集合",required=true)
	})
	@DeleteMapping(value="/{goods_ids}")
	public String cleanGoods(@PathVariable("goods_ids") Long[] goodsIds){

		this.goodsManager.delete(goodsIds);

		return "";
	}


	@GetMapping(value = "/{goods_ids}/details")
	@Operation(summary = "查询多个商品的基本信息")
	@Parameters({
			@Parameter(name = "goods_ids", description = "要查询的商品主键", required = true,  in = ParameterIn.PATH),
			@Parameter(name = "param", description = "查询条件参数",  in = ParameterIn.QUERY)
	})
	public List<GoodsSelectLine> getGoodsDetail(@PathVariable("goods_ids") Long[] goodsIds, GoodsQueryParam param) {

		Seller seller = UserContext.getSeller();
		param.setSellerId(seller.getSellerId());
		return this.goodsQueryManager.queryGoodsLines(goodsIds, param);
	}

}
