/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.seller.orderbill;

import cn.shoptnt.model.goods.enums.Permission;
import cn.shoptnt.model.support.LogClient;
import cn.shoptnt.model.support.validator.annotation.Log;
import cn.shoptnt.service.orderbill.validator.annotation.BillItemType;
import cn.shoptnt.model.orderbill.dos.Bill;
import cn.shoptnt.model.orderbill.vo.BillDetail;
import cn.shoptnt.model.orderbill.vo.BillExcel;
import cn.shoptnt.model.orderbill.vo.BillQueryParam;
import cn.shoptnt.service.orderbill.BillItemManager;
import cn.shoptnt.service.orderbill.BillManager;
import cn.shoptnt.framework.context.user.UserContext;
import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.framework.security.model.Seller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


/**
 * @author fk
 * @version v1.0
 * @Description: 结算单账单控制器
 * @date 2018/4/27 11:49
 * @since v7.0.0
 */
@RestController
@RequestMapping("/seller/order/bills")
@Tag(name = "结算相关API")
@Validated
public class OrderBillSellerController {

    @Autowired
    private BillManager billManager;
    @Autowired
    private BillItemManager billItemManager;

    @Operation(summary = "商家查看我的账单列表")
    @Parameters({
            @Parameter(name = "page_no", description = "页码",  in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页数量",  in = ParameterIn.QUERY),
            @Parameter(name = "bill_sn", description = "结算编号",   in = ParameterIn.QUERY)
    })
    @GetMapping
    public WebPage queryBillList(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize, @Parameter(hidden = true) String billSn) {

        Seller seller = UserContext.getSeller();
        BillQueryParam param = new BillQueryParam();
        param.setPageNo(pageNo);
        param.setPageSize(pageSize);
        param.setSellerId(seller.getSellerId());
        param.setBillSn(billSn);

        return this.billManager.queryBills(param);
    }

    @Operation(summary = "商家查看某账单详细")
    @Parameters({
            @Parameter(name = "bill_id", description = "结算单id", required = true,  in = ParameterIn.PATH),
    })
    @GetMapping("/{bill_id}")
    public BillDetail queryBill(@PathVariable("bill_id") Long billId) {

        return this.billManager.getBillDetail(billId, Permission.SELLER);
    }


    @Operation(summary = "卖家对账单进行下一步操作")
    @Parameters({
            @Parameter(name = "bill_id", description = "账单id", required = true,  in = ParameterIn.PATH),
    })
    @PutMapping(value = "/{bill_id}/next")
    @Log(client = LogClient.seller,detail = "商家对账，结算单号：${billId}")
    public Bill nextBill(@PathVariable("bill_id") Long billId) {

        Bill bill = this.billManager.editStatus(billId, Permission.SELLER);

        return bill;
    }

    @Operation(summary = "查看账单中的订单列表或者退款单列表")
    @Parameters({
            @Parameter(name = "page_no", description = "页码",  in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页数量",  in = ParameterIn.QUERY),
            @Parameter(name = "bill_id", description = "账单id", required = true,  in = ParameterIn.PATH),
            @Parameter(name = "bill_type", description = "账单类型", required = true,   in = ParameterIn.PATH),
    })
    @GetMapping("/{bill_id}/{bill_type}")
    public WebPage queryBillItems(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize, @PathVariable("bill_id") Long billId, @BillItemType @PathVariable("bill_type") String billType) {

        return this.billItemManager.list(pageNo, pageSize, billId, billType);
    }

    @Operation(summary = "导出某账单详细")
    @Parameters({
            @Parameter(name = "bill_id", description = "结算单id", required = true,  in = ParameterIn.PATH),
    })
    @GetMapping("/{bill_id}/export")
    public BillExcel exportBill(@PathVariable("bill_id") Long billId) {

        return  this.billManager.exportBill(billId);
    }

}
