/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.seller.promotion;

import cn.shoptnt.framework.validation.annotation.SafeDomain;
import cn.shoptnt.model.promotion.fulldiscount.dos.FullDiscountGiftDO;
import cn.shoptnt.service.promotion.fulldiscount.FullDiscountGiftManager;
import cn.shoptnt.framework.context.user.UserContext;
import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.framework.exception.NoPermissionException;
import cn.shoptnt.framework.security.model.Seller;
import cn.shoptnt.framework.util.CurrencyUtil;
import cn.shoptnt.framework.util.DateUtil;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 满减满赠促销活动赠品控制器
 *
 * @author Snow
 * @version v7.0.0
 * @since v7.0.0
 * 2018-03-30 17:34:46
 */
@SuppressWarnings("Duplicates")
@RestController
@RequestMapping("/seller/promotion/full-discount-gifts")
@Tag(name = "满减满赠促销活动赠品相关API")
@Validated
public class FullDiscountGiftSellerController {

    @Autowired
    private FullDiscountGiftManager fullDiscountGiftManager;

    @Operation(summary = "查询赠品信息分页列表数据")
    @Parameters({
            @Parameter(name = "page_no", description = "页码", in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量", in = ParameterIn.QUERY),
            @Parameter(name = "keyword", description = "关键字查询", in = ParameterIn.QUERY)
    })
    @GetMapping
    public WebPage list(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize, String keyword) {

        return this.fullDiscountGiftManager.list(pageNo, pageSize, keyword);
    }


    @Operation(summary = "添加赠品信息")
    @Parameters({
            @Parameter(name = "gift_name", description = "赠品名称", required = true, in = ParameterIn.QUERY),
            @Parameter(name = "gift_price", description = "赠品金额", required = true, in = ParameterIn.QUERY),
            @Parameter(name = "gift_img", description = "赠品图片", required = true, in = ParameterIn.QUERY),
            @Parameter(name = "actual_store", description = "库存", required = true, in = ParameterIn.QUERY),
    })
    @PostMapping
    public FullDiscountGiftDO add(@Parameter(hidden = true) @NotEmpty(message = "请填写赠品名称") String giftName,
                                  @Parameter(hidden = true) @NotNull(message = "请填写赠品金额") Double giftPrice,
                                  @Parameter(hidden = true) @NotEmpty(message = "请上传赠品图片") @SafeDomain String giftImg,
                                  @Parameter(hidden = true) @NotNull(message = "请填写库存") Integer actualStore) {
        //获取当前登录的商家信息
        Seller seller = UserContext.getSeller();

        //创建赠品实体对象
        FullDiscountGiftDO giftDO = new FullDiscountGiftDO();
        //设置赠品名称
        giftDO.setGiftName(giftName);
        //设置赠品价格
        giftDO.setGiftPrice(giftPrice);
        //设置赠品图片
        giftDO.setGiftImg(giftImg);
        //设置赠品实际库存
        giftDO.setActualStore(actualStore);
        //设置赠品可用库存(添加时可用库存=实际库存)
        giftDO.setEnableStore(actualStore);
        //添加赠品创建时间
        giftDO.setCreateTime(DateUtil.getDateline());
        //设置赠品类型(1.0版本赠品类型默认为0：普通赠品)
        giftDO.setGiftType(0);
        //默认不禁用
        giftDO.setDisabled(0);
        //设置赠品所属店铺id
        giftDO.setSellerId(seller.getSellerId());
        //添加赠品信息
        this.fullDiscountGiftManager.add(giftDO);

        return giftDO;
    }

    @PutMapping(value = "/{gift_id}")
    @Operation(summary = "修改赠品信息")
    @Parameters({
            @Parameter(name = "gift_id", description = "赠品id", required = true, in = ParameterIn.PATH),
            @Parameter(name = "gift_name", description = "赠品名称", required = true, in = ParameterIn.QUERY),
            @Parameter(name = "gift_name", description = "赠品名称", required = true, in = ParameterIn.QUERY),
            @Parameter(name = "gift_price", description = "赠品金额", required = true, in = ParameterIn.QUERY),
            @Parameter(name = "gift_img", description = "赠品图片", required = true, in = ParameterIn.QUERY),
            @Parameter(name = "actual_store", description = "库存", required = true, in = ParameterIn.QUERY),
    })
    public FullDiscountGiftDO edit(@Parameter(hidden = true) @PathVariable("gift_id") Long giftId,
                                   @Parameter(hidden = true) @NotEmpty(message = "请填写赠品名称") String giftName,
                                   @Parameter(hidden = true) @NotNull(message = "请填写赠品金额") Double giftPrice,
                                   @Parameter(hidden = true) @NotEmpty(message = "请上传赠品图片") @SafeDomain String giftImg,
                                   @Parameter(hidden = true) @NotNull(message = "请填写库存") Integer actualStore) {
        //操作权限校验（如果当前修改的赠品不属于当前操作的商家则不允许操作）
        this.fullDiscountGiftManager.verifyAuth(giftId);
        //获取原赠品信息
        FullDiscountGiftDO fullDiscountGift = fullDiscountGiftManager.getModel(giftId);
        //获取原被占用库存 = 实际库存-可用库存
        double storeSub = CurrencyUtil.sub(fullDiscountGift.getActualStore(), fullDiscountGift.getEnableStore());
        //获取实际可用库存 = 新库存 - 被占用库存
        double enableStore = CurrencyUtil.sub(actualStore, storeSub);
        //获取当前登录商家信息
        Seller seller = UserContext.getSeller();

        //创建赠品实体对象
        FullDiscountGiftDO giftDO = new FullDiscountGiftDO();
        //设置赠品主键ID
        giftDO.setGiftId(giftId);
        //设置赠品名称
        giftDO.setGiftName(giftName);
        //设置赠品价格
        giftDO.setGiftPrice(giftPrice);
        //设置赠品图片
        giftDO.setGiftImg(giftImg);
        //设置赠品实际库存
        giftDO.setActualStore(actualStore);
        //设置赠品可用库存(添加时可用库存=实际库存)
        giftDO.setEnableStore((int) enableStore);
        //添加赠品创建时间
        giftDO.setCreateTime(DateUtil.getDateline());
        //设置赠品类型(1.0版本赠品类型默认为0：普通赠品)
        giftDO.setGiftType(0);
        //默认不禁用
        giftDO.setDisabled(0);
        //设置赠品所属店铺id
        giftDO.setSellerId(seller.getSellerId());
        //修改赠品信息
        this.fullDiscountGiftManager.edit(giftDO, giftId);

        return giftDO;
    }


    @DeleteMapping(value = "/{id}")
    @Operation(summary = "删除赠品信息")
    @Parameters({
            @Parameter(name = "id", description = "要删除的赠品主键ID", required = true, in = ParameterIn.PATH)
    })
    public String delete(@PathVariable Long id) {
        //操作权限校验（如果当前修改的赠品不属于当前操作的商家则不允许操作）
        this.fullDiscountGiftManager.verifyAuth(id);
        //如果赠品参与了满减满赠促销活动并且活动正在进行中，则不允许删除
        this.fullDiscountGiftManager.verifyGift(id);
        //删除赠品信息
        this.fullDiscountGiftManager.delete(id);
        return "";
    }


    @GetMapping(value = "/{id}")
    @Operation(summary = "查询一个赠品新增")
    @Parameters({
            @Parameter(name = "id", description = "要查询的赠品主键ID", required = true, in = ParameterIn.PATH)
    })
    public FullDiscountGiftDO get(@PathVariable Long id) {
        //根据ID获取赠品信息
        FullDiscountGiftDO fullDiscountGift = this.fullDiscountGiftManager.getModel(id);
        //获取当前登录的商家信息
        Seller seller = UserContext.getSeller();
        //验证越权操作（赠品信息为空或者赠品不属于当前登录的商家，提示无权操作）
        if (fullDiscountGift == null || !seller.getSellerId().equals(fullDiscountGift.getSellerId())) {
            throw new NoPermissionException("无权操作");
        }

        return fullDiscountGift;
    }

    @Operation(summary = "查询当前登录商家的赠品信息集合")
    @GetMapping(value = "/all")
    public List<FullDiscountGiftDO> listAll() {
        return this.fullDiscountGiftManager.listAll();
    }

}
