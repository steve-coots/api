/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.seller.aftersale;

import cn.shoptnt.model.aftersale.dto.RefundQueryParam;
import cn.shoptnt.model.aftersale.enums.ServiceOperateTypeEnum;
import cn.shoptnt.service.aftersale.AfterSaleRefundManager;
import cn.shoptnt.framework.context.user.UserContext;
import cn.shoptnt.framework.database.WebPage;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;

/**
 * 售后退款相关API
 *
 * @author duanmingyu
 * @version v1.0
 * @since v7.1.5
 * 2019-10-28
 */
@Tag(name = "售后退款相关API")
@RestController
@RequestMapping("/seller/after-sales/refund")
@Validated
public class RefundSellerController {

    @Autowired
    private AfterSaleRefundManager afterSaleRefundManager;

    @Operation(summary = "获取售后退款单列表")
    @GetMapping()
    public WebPage list(@Valid RefundQueryParam param) {
        param.setSellerId(UserContext.getSeller().getSellerId());
        return afterSaleRefundManager.list(param);
    }

    @Operation(summary = "在线支付订单商家退款")
    @Parameters({
            @Parameter(name = "service_sn", description = "售后服务单号", required = true, in = ParameterIn.PATH)
    })
    @PostMapping(value = "/{service_sn}")
    public void refund(@PathVariable("service_sn") String serviceSn) {
        this.afterSaleRefundManager.sellerRefund(serviceSn);
    }

    @Operation(summary = "货到付款订单商家退款")
    @Parameters({
            @Parameter(name = "service_sn", description = "售后退款单号", required = true, in = ParameterIn.PATH),
            @Parameter(name = "refund_price", description = "退款金额", required = true, in = ParameterIn.QUERY),
            @Parameter(name = "remark", description = "售后退款备注", in = ParameterIn.QUERY)
    })
    @PostMapping(value = "/cod/{service_sn}")
    public void codOrderRefund(@PathVariable("service_sn")@Parameter(hidden = true) String serviceSn,@Parameter(hidden = true) Double refundPrice, @Parameter(hidden = true) String remark) {
        this.afterSaleRefundManager.adminRefund(serviceSn, refundPrice, remark, ServiceOperateTypeEnum.SELLER_REFUND);
    }
}
