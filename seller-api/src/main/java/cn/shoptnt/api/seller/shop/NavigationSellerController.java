/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.seller.shop;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;

import cn.shoptnt.framework.context.user.UserContext;
import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.framework.security.model.Seller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;


import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import cn.shoptnt.model.shop.dos.NavigationDO;
import cn.shoptnt.model.shop.vo.operator.SellerEditShop;
import cn.shoptnt.service.shop.NavigationManager;

/**
 * 店铺导航管理控制器
 *
 * @author zjp
 * @version v7.0.0
 * @since v7.0.0
 * 2018-03-28 20:44:54
 */
@RestController
@RequestMapping("/seller/shops/navigations")
@Tag(name = "店铺导航管理相关API")
public class NavigationSellerController {

    @Autowired
    private NavigationManager navigationManager;


    @Operation(summary = "查询店铺导航管理列表")
    @Parameters({
            @Parameter(name = "page_no", description = "页码", required = true, in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量", required = true, in = ParameterIn.QUERY)
    })
    @GetMapping
    public WebPage list(@Parameter(hidden = true) @NotNull(message = "页码不能为空") Long pageNo, @Parameter(hidden = true) @NotNull(message = "每页数量不能为空") Long pageSize) {
        return this.navigationManager.list(pageNo, pageSize, UserContext.getSeller().getSellerId());
    }


    @Operation(summary = "添加店铺导航")
    @PostMapping
    public NavigationDO add(@Valid NavigationDO navigation) {
        Seller seller = UserContext.getSeller();
        navigation.setShopId(seller.getSellerId());
        this.navigationManager.add(navigation);
        return navigation;
    }

    @PutMapping(value = "/{id}")
    @Operation(summary = "修改店铺导航")
    @Parameters({
            @Parameter(name = "id", description = "主键", required = true, in = ParameterIn.PATH)
    })
    public NavigationDO edit(@Valid NavigationDO navigation, @PathVariable Long id) {

        this.navigationManager.edit(navigation, id);

        return navigation;
    }


    @DeleteMapping(value = "/{id}")
    @Operation(summary = "删除店铺导航")
    @Parameters({
            @Parameter(name = "id", description = "要删除的店铺导航管理主键", required = true, in = ParameterIn.PATH)
    })
    public SellerEditShop delete(@PathVariable Long id) {
        Seller seller = UserContext.getSeller();
        SellerEditShop sellerEditShop = new SellerEditShop();
        sellerEditShop.setSellerId(seller.getSellerId());
        sellerEditShop.setOperator("删除店铺导航");
        this.navigationManager.delete(id);
        return sellerEditShop;
    }

}
