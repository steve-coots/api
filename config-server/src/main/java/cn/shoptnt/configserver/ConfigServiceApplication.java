/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.configserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;

/**
 * Created by 妙贤 on 2018/3/9.
 * 配置中心应用
 *
 * @author 妙贤
 * @version 1.0
 * @since 7.0.0
 * 2018/3/9
 */
@EnableConfigServer
@SpringBootApplication
public class
ConfigServiceApplication {
    public static void main(String[] args) {
        SpringApplication.run(ConfigServiceApplication.class, args);
    }
}

