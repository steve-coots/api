/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.system.vo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import cn.shoptnt.model.base.vo.ConfigItem;
import cn.shoptnt.model.system.dos.ValidatorPlatformDO;
import cn.shoptnt.framework.database.annotation.Column;
import cn.shoptnt.framework.database.annotation.Id;
import com.google.common.reflect.TypeToken;
import com.google.gson.Gson;
import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;
import java.util.List;

/**
 * 验证平台实体VO
 * @author duanmingyu
 * @version v1.0
 * @since v7.1.6
 * 2019-12-18
 */
public class ValidatorPlatformVO implements Serializable {

    private static final long serialVersionUID = 7430241246638433536L;

    /**
     * 验证平台id
     */
    @TableId(type= IdType.ASSIGN_ID)
    @Schema(hidden = true)
    private Long id;
    /**
     * 验证平台名称
     */
    @Column(name = "name")
    @Schema(name = "name", description = "验证平台名称")
    private String name;
    /**
     * 是否开启验证平台,1开启，0未开启
     */
    @Column(name = "open")
    @Schema(name = "open", description = "是否开启验证平台,1开启，0未开启")
    private Integer open;
    /**
     * 验证平台配置
     */
    @Column(name = "config")
    @Schema(name = "config", description = "验证平台配置")
    private String config;
    /**
     * 验证平台插件ID
     */
    @Column(name = "plugin_id")
    @Schema(name = "plugin_id", description = "验证平台插件ID")
    private String pluginId;

    /**
     * 滑块验证平台配置项
     */
    @Schema(name = "configItems", description = "滑块验证配置项", required = true)
    private List<ConfigItem> configItems;

    public ValidatorPlatformVO() {

    }

    public ValidatorPlatformVO(ValidatorPlatformDO validatorPlatformDO) {
        this.id = validatorPlatformDO.getId();
        this.name = validatorPlatformDO.getName();
        this.open = validatorPlatformDO.getOpen();
        this.config = validatorPlatformDO.getConfig();
        this.pluginId = validatorPlatformDO.getPluginId();
        Gson gson = new Gson();
        this.configItems = gson.fromJson(validatorPlatformDO.getConfig(), new TypeToken<List<ConfigItem>>() {
        }.getType());
    }



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getOpen() {
        return open;
    }

    public void setOpen(Integer open) {
        this.open = open;
    }

    public String getConfig() {
        return config;
    }

    public void setConfig(String config) {
        this.config = config;
    }

    public String getPluginId() {
        return pluginId;
    }

    public void setPluginId(String pluginId) {
        this.pluginId = pluginId;
    }

    public List<ConfigItem> getConfigItems() {
        return configItems;
    }

    public void setConfigItems(List<ConfigItem> configItems) {
        this.configItems = configItems;
    }

    @Override
    public String toString() {
        return "ValidatorPlatformVO{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", open=" + open +
                ", config='" + config + '\'' +
                ", pluginId='" + pluginId + '\'' +
                ", configItems=" + configItems +
                '}';
    }
}
