/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.system.vo;

import cn.shoptnt.framework.database.annotation.Column;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;


/**
 * 地区实体
 *
 * @author zh
 * @version v7.0.0
 * @since v7.0.0
 * 2018-04-28 13:49:38
 */
public class RegionsVO {

    /**
     * 父地区id
     */
    @Column(name = "parent_id")
    @Min(message = "必须为数字", value = 0)
    @NotNull(message = "父id不能为空")
    @Schema(name = "parent_id", description =  "父地区id，顶级分类填0", required = true)
    private Long parentId;
    /**
     * 名称
     */
    @Column(name = "local_name")
    @Schema(name = "local_name", description =  "名称")
    @NotEmpty(message = "地区名称不能为空")
    private String localName;
    /**
     * 邮编
     */
    @Column(name = "zipcode")
    @Schema(name = "zipcode", description =  "邮编")
    private String zipcode;
    /**
     * 是否支持货到付款
     */
    @Column(name = "cod")
    @Schema(name = "cod", description =  "是否支持货到付款,1支持,0不支持")
    @Min(message = "是否支持货到付款,1支持,0不支持", value = 0)
    @Max(message = "是否支持货到付款,1支持,0不支持", value = 1)
    @NotNull(message = "是否支持货到付款不能为空")
    private Integer cod;


    public Long getParentId() {
        return parentId;
    }

    public void setParentId(Long parentId) {
        this.parentId = parentId;
    }

    public String getLocalName() {
        return localName;
    }

    public void setLocalName(String localName) {
        this.localName = localName;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public Integer getCod() {
        return cod;
    }

    public void setCod(Integer cod) {
        this.cod = cod;
    }

    @Override
    public String toString() {
        return "RegionsVO{" +
                "parentId=" + parentId +
                ", localName='" + localName + '\'' +
                ", zipcode='" + zipcode + '\'' +
                ", cod=" + cod +
                '}';
    }
}
