/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.statistics.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;

/**
 * 平台PV
 *
 * @author 张崧
 * @since 2024-04-24
 */
@TableName("es_sss_platform_pv")
public class PlatformPageView implements Serializable {

    @TableId(type = IdType.ASSIGN_ID)
    private Long id;

    @Schema(name = "year", description = "访问年份")
    private Integer year;

    @Schema(name = "month", description = "访问月份")
    private Integer month;

    @Schema(name = "day", description = "访问日")
    private Integer day;

    @Schema(name = "num", description = "访问次数")
    private Integer num;

    @Schema(name = "time", description = "统计日期的时间戳")
    private Long time;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public Integer getMonth() {
        return month;
    }

    public void setMonth(Integer month) {
        this.month = month;
    }

    public Integer getDay() {
        return day;
    }

    public void setDay(Integer day) {
        this.day = day;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Long getTime() {
        return time;
    }

    public void setTime(Long time) {
        this.time = time;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;

        if (o == null || getClass() != o.getClass()) return false;

        PlatformPageView that = (PlatformPageView) o;

        return new EqualsBuilder().append(id, that.id).append(year, that.year).append(month, that.month).append(day, that.day).append(num, that.num).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(id).append(year).append(month).append(day).append(num).toHashCode();
    }

    @Override
    public String toString() {
        return "PlatformPageView{" +
                "id=" + id +
                ", year=" + year +
                ", month=" + month +
                ", day=" + day +
                ", num=" + num +
                '}';
    }
}
