/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.member.dto;

import cn.shoptnt.model.base.context.Region;
import cn.shoptnt.model.base.context.RegionFormat;
import cn.shoptnt.framework.database.annotation.Column;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;


/**
 * 会员参数传递
 *
 * @author zh
 * @version v7.0.0
 * @since v7.0.0
 * 2018-03-30 14:27:48
 */
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class MemberQueryParam {
    /**
     * 会员登录用户名
     */
    @Schema(name = "uname", description = "会员登录用户名")
    private String uname;
    /**
     * 邮箱
     */
    @Schema(name = "email", description = "邮箱")
    private String email;
    /**
     * 会员手机号码
     */
    @Schema(name = "mobile", description = "会员手机号码")
    private String mobile;
    /**
     * 会员性别
     */
    @Schema(name = "sex", description = "会员性别,1为男，0为女")
    private Integer sex;

    @RegionFormat
    @Schema(name = "region", description = "地区")
    private Region region;
    /**
     * 开始时间
     */
    @Schema(name = "start_time", description = "开始时间")
    private String startTime;
    /**
     * 结束时间
     */
    @Schema(name = "end_time", description = "结束时间")
    private String endTime;
    /**
     * 关键字
     */
    @Schema(name = "keyword", description = "关键字")
    private String keyword;
    /**
     * 页码
     */
    @Schema(name = "page_no", description = "页码", required = true)
    private Long pageNo;

    /**
     * 会员状态
     */
    @Schema(name = "disabled", description = "会员状态,0为正常.-1为删除，在会员回收站中")
    private Integer disabled;

    /**
     * 分页数
     */
    @Schema(name = "page_size", description = "分页数", required = true)
    private Long pageSize;

    public String getUname() {
        return uname;
    }

    public void setUname(String uname) {
        this.uname = uname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public Long getPageNo() {
        return pageNo;
    }

    public void setPageNo(Long pageNo) {
        this.pageNo = pageNo;
    }

    public Integer getDisabled() {
        return disabled;
    }

    public void setDisabled(Integer disabled) {
        this.disabled = disabled;
    }

    public Long getPageSize() {
        return pageSize;
    }

    public void setPageSize(Long pageSize) {
        this.pageSize = pageSize;
    }

    @Override
    public String toString() {
        return "MemberQueryParam{" +
                "uname='" + uname + '\'' +
                ", email='" + email + '\'' +
                ", mobile='" + mobile + '\'' +
                ", sex=" + sex +
                ", region=" + region +
                ", startTime='" + startTime + '\'' +
                ", endTime='" + endTime + '\'' +
                ", keyword='" + keyword + '\'' +
                ", pageNo=" + pageNo +
                ", disabled=" + disabled +
                ", pageSize=" + pageSize +
                '}';
    }
}