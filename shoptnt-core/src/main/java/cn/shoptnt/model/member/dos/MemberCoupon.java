/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.member.dos;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import cn.shoptnt.model.promotion.coupon.dos.CouponDO;
import cn.shoptnt.framework.database.annotation.PrimaryKeyField;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

import java.io.Serializable;


/**
 * 会员优惠券实体
 *
 * @author Snow
 * @version vv7.0.0
 * @since v7.0.0
 * 2018-06-12 21:48:46
 */
@TableName(value = "es_member_coupon")
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class MemberCoupon implements Serializable {

    private static final long serialVersionUID = 5545788652245350L;

    /**
     * 主键
     */
    @TableId(type = IdType.ASSIGN_ID)
    @Schema(hidden = true)
    private Long mcId;

    /**
     * 优惠券表主键
     */
    @Schema(name = "coupon_id", description =  "优惠券表主键")
    private Long couponId;

    /**
     * 会员表主键
     */
    @Schema(name = "member_id", description =  "会员表主键")
    private Long memberId;

    /**
     * 使用时间
     */
    @Schema(name = "used_time", description =  "使用时间")
    private Long usedTime;

    /**
     * 领取时间
     */
    @Schema(name = "create_time", description =  "领取时间")
    private Long createTime;

    /**
     * 订单表主键
     */
    @Schema(name = "order_id", description =  "订单表主键")
    private Long orderId;

    /**
     * 订单编号
     */
    @Schema(name = "order_sn", description =  "订单编号")
    private String orderSn;

    /**
     * 会员用户名
     */
    @Schema(name = "member_name", description =  "会员用户名")
    private String memberName;

    /**
     * 优惠券名称
     */
    @Schema(name = "title", description =  "优惠券名称")
    private String title;

    /**
     * 优惠券面额
     */
    @Schema(name = "coupon_price", description =  "优惠券面额")
    private Double couponPrice;

    /**
     * 优惠券门槛金额
     */
    @Schema(name = "coupon_threshold_price", description =  "优惠券门槛金额")
    private Double couponThresholdPrice;

    /**
     * 有效期--起始时间
     */
    @Schema(name = "start_time", description =  "有效期--起始时间")
    private Long startTime;

    /**
     * 有效期--截止时间
     */
    @Schema(name = "end_time", description =  "有效期--截止时间")
    private Long endTime;

    /**
     * 使用状态 0:未使用,1:已使用,2:已过期,3:已作废
     */
    @Schema(name = "used_status", description =  "使用状态", example = "0:未使用,1:已使用,2:已过期,3:已作废")
    private Integer usedStatus;

    /**
     * 商家ID
     */
    @Schema(name = "seller_id", description =  "商家ID")
    private Long sellerId;

    /**
     * 商家名称
     */
    @Schema(name = "seller_name", description =  "商家名称")
    private String sellerName;

    /**
     * 使用状态文字（非数据库字段）
     */
    @Schema(description =  "使用状态文字")
    @TableField(exist = false)
    private String usedStatusText;

    /**
     * 	使用范围 ALL:全品,CATEGORY:分类,SOME_GOODS:部分商品
     */
    @Schema(name = "use_scope", description =  "使用范围 ALL:全品,CATEGORY:分类,SOME_GOODS:部分商品")
    private String useScope;

    /**
     * 范围关联的id
     * 全品或者商家优惠券时为0
     * 分类时为分类id
     * 部分商品时为商品ID集合
     */
    @Schema(name = "scope_id", description =  "范围关联的id")
    private String scopeId;


    public MemberCoupon() {
    }

    public MemberCoupon(CouponDO couponDO) {
        this.setCouponId(couponDO.getCouponId());
        this.setTitle(couponDO.getTitle());
        this.setStartTime(couponDO.getStartTime());
        this.setEndTime(couponDO.getEndTime());
        this.setCouponPrice(couponDO.getCouponPrice());
        this.setCouponThresholdPrice(couponDO.getCouponThresholdPrice());
        this.setSellerId(couponDO.getSellerId());
        this.setSellerName(couponDO.getSellerName());
        this.setUseScope(couponDO.getUseScope());
        this.setScopeId(couponDO.getScopeId());
    }

    @PrimaryKeyField
    public Long getMcId() {
        return mcId;
    }

    public void setMcId(Long mcId) {
        this.mcId = mcId;
    }

    public Long getCouponId() {
        return couponId;
    }

    public void setCouponId(Long couponId) {
        this.couponId = couponId;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public Long getUsedTime() {
        return usedTime;
    }

    public void setUsedTime(Long usedTime) {
        this.usedTime = usedTime;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public String getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Double getCouponPrice() {
        return couponPrice;
    }

    public void setCouponPrice(Double couponPrice) {
        this.couponPrice = couponPrice;
    }

    public Double getCouponThresholdPrice() {
        return couponThresholdPrice;
    }

    public void setCouponThresholdPrice(Double couponThresholdPrice) {
        this.couponThresholdPrice = couponThresholdPrice;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public Integer getUsedStatus() {
        return usedStatus;
    }

    public void setUsedStatus(Integer usedStatus) {
        this.usedStatus = usedStatus;
    }

    public String getUsedStatusText() {
        if (usedStatus == 0) {
            usedStatusText = "未使用";
        } else if(usedStatus == 2){
            usedStatusText = "已过期";
        }else{
            usedStatusText = "已使用";
        }

        return usedStatusText;
    }

    public void setUsedStatusText(String usedStatusText) {
        this.usedStatusText = usedStatusText;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public String getSellerName() {
        return sellerName;
    }

    public void setSellerName(String sellerName) {
        this.sellerName = sellerName;
    }


    public String getUseScope() {
        return useScope;
    }

    public void setUseScope(String useScope) {
        this.useScope = useScope;
    }

    public String getScopeId() {
        return scopeId;
    }

    public void setScopeId(String scopeId) {
        this.scopeId = scopeId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        MemberCoupon that = (MemberCoupon) o;

        return new EqualsBuilder()
                .append(mcId, that.mcId)
                .append(couponId, that.couponId)
                .append(memberId, that.memberId)
                .append(usedTime, that.usedTime)
                .append(createTime, that.createTime)
                .append(orderId, that.orderId)
                .append(orderSn, that.orderSn)
                .append(memberName, that.memberName)
                .append(title, that.title)
                .append(couponPrice, that.couponPrice)
                .append(couponThresholdPrice, that.couponThresholdPrice)
                .append(startTime, that.startTime)
                .append(endTime, that.endTime)
                .append(usedStatus, that.usedStatus)
                .append(sellerId, that.sellerId)
                .append(sellerName, that.sellerName)
                .append(usedStatusText, that.usedStatusText)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(mcId)
                .append(couponId)
                .append(memberId)
                .append(usedTime)
                .append(createTime)
                .append(orderId)
                .append(orderSn)
                .append(memberName)
                .append(title)
                .append(couponPrice)
                .append(couponThresholdPrice)
                .append(startTime)
                .append(endTime)
                .append(usedStatus)
                .append(sellerId)
                .append(sellerName)
                .append(usedStatusText)
                .toHashCode();
    }

    @Override
    public String toString() {
        return "MemberCoupon{" +
                "mcId=" + mcId +
                ", couponId=" + couponId +
                ", memberId=" + memberId +
                ", usedTime=" + usedTime +
                ", createTime=" + createTime +
                ", orderId=" + orderId +
                ", orderSn='" + orderSn + '\'' +
                ", memberName='" + memberName + '\'' +
                ", title='" + title + '\'' +
                ", couponPrice=" + couponPrice +
                ", couponThresholdPrice=" + couponThresholdPrice +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", usedStatus=" + usedStatus +
                ", sellerId=" + sellerId +
                ", sellerName='" + sellerName + '\'' +
                ", usedStatusText='" + usedStatusText + '\'' +
                '}';
    }
}
