/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.member.dto;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;

/**
 * 会员站内消息DTO
 * @author duanmingyu
 * @version v1.0
 * @since V7.1.5
 * 2019-09-24
 */
public class MemberNoticeDTO implements Serializable {

    private static final long serialVersionUID = 2958965751114746887L;

    @Schema(name = "total", description =  "消息总数")
    private Integer total;
    @Schema(name = "system_num", description =  "系统消息总数")
    private Integer systemNum;
    @Schema(name = "ask_num", description =  "问答消息总数")
    private Integer askNum;

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Integer getSystemNum() {
        return systemNum;
    }

    public void setSystemNum(Integer systemNum) {
        this.systemNum = systemNum;
    }

    public Integer getAskNum() {
        return askNum;
    }

    public void setAskNum(Integer askNum) {
        this.askNum = askNum;
    }

    @Override
    public String toString() {
        return "MemberNoticeDTO{" +
                "total=" + total +
                ", systemNum=" + systemNum +
                ", askNum=" + askNum +
                '}';
    }
}
