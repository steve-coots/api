/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.member.vo;

import cn.shoptnt.framework.database.annotation.Column;
import cn.shoptnt.framework.util.StringUtil;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;

/**
 * 销售记录VO
 *
 * @author chopper
 * @version v1.0
 * @since v7.0
 * 2018-06-29 上午9:32
 */
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class SalesVO implements Serializable {
    @Schema(name="buyer_name",description="买家")
    private String buyerName;

    @Schema(name="price",description="价格")
    private Double price;

    @Schema(name="num",description="数量")
    private Integer num;

    @Schema(name="create_time",description="购买日期")
    private Integer createTime;


    public String getBuyerName() {
        return buyerName;
    }

    public void setBuyerName(String buyerName) {
        if(!StringUtil.isEmpty(buyerName)){
            if(buyerName.length()==1){
                this.buyerName="***";
            }else {
                this.buyerName = buyerName.substring(0, 1) + "***" + buyerName.substring(buyerName.length() - 1, buyerName.length());
            }
        }
    }
    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Integer getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Integer createTime) {
        this.createTime = createTime;
    }

    @Override
    public String toString() {
        return "SalesVO{" +
                "buyerName='" + buyerName + '\'' +
                ", price=" + price +
                ", num=" + num +
                ", createTime=" + createTime +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        SalesVO salesVO = (SalesVO) o;

        if (buyerName != null ? !buyerName.equals(salesVO.buyerName) : salesVO.buyerName != null) {
            return false;
        }
        if (price != null ? !price.equals(salesVO.price) : salesVO.price != null) {
            return false;
        }
        if (num != null ? !num.equals(salesVO.num) : salesVO.num != null) {
            return false;
        }
        return createTime != null ? createTime.equals(salesVO.createTime) : salesVO.createTime == null;
    }

    @Override
    public int hashCode() {
        int result = buyerName != null ? buyerName.hashCode() : 0;
        result = 31 * result + (price != null ? price.hashCode() : 0);
        result = 31 * result + (num != null ? num.hashCode() : 0);
        result = 31 * result + (createTime != null ? createTime.hashCode() : 0);
        return result;
    }
}
