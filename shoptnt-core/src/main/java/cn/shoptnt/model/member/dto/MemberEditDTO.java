/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.member.dto;

import cn.shoptnt.framework.validation.annotation.SafeDomain;
import cn.shoptnt.model.base.context.Region;
import cn.shoptnt.model.base.context.RegionFormat;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

/**
 * 买家修改会员DTO
 *
 * @author zh
 * @version v7.0
 * @date 18/4/26 下午10:40
 * @since v7.0
 */
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class MemberEditDTO {

    /**
     * 昵称
     */
    @Schema(name = "nickname",description =  "昵称", required = true)
    @Length(min = 2, max = 20, message = "会员昵称必须为2到20位之间")
    private String nickname;

    @RegionFormat
    @Schema(name = "region",description =  "地区")
    private Region region;

    /**
     * 会员性别
     */
    @Min(message = "必须为数字且1为男,0为女",value =  0)
    @Max(message = "必须为数字且1为男,0为女", value = 1)
    @NotNull(message = "会员性别不能为空")
    @Schema(name = "sex",description =  "会员性别,1为男，0为女", required = true)
    private Integer sex;
    /**
     * 会员生日
     */
    @Schema(name = "birthday",description =  "会员生日")
    private Long birthday;
    /**
     * 详细地址
     */
    @Schema(name = "address",description =  "详细地址")
    private String address;
    /**
     * 邮箱
     */
    @Email(message = "邮箱格式不正确")
    @Schema(name = "email",description =  "邮箱")
    private String email;

    /**
     * 座机号码
     */
    @Schema(name = "tel", description = "座机号码")
    private String tel;

    /**
     * 会员头像
     */
    @SafeDomain
    @Schema(name = "face", description = "会员头像")
    private String face;

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public Integer getSex() {
        return sex;
    }

    public void setSex(Integer sex) {
        this.sex = sex;
    }

    public Long getBirthday() {
        return birthday;
    }

    public void setBirthday(Long birthday) {
        this.birthday = birthday;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getFace() {
        return face;
    }

    public void setFace(String face) {
        this.face = face;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }


    @Override
    public String toString() {
        return "MemberEditDTO{" +
                "nickname='" + nickname + '\'' +
                ", region=" + region +
                ", sex=" + sex +
                ", birthday=" + birthday +
                ", address='" + address + '\'' +
                ", email='" + email + '\'' +
                ", tel='" + tel + '\'' +
                ", face='" + face + '\'' +
                '}';
    }
}
