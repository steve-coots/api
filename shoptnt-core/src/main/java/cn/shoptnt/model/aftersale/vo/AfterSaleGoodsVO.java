/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.aftersale.vo;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;

/**
 * 售后商品信息VO
 * @author duanmingyu
 * @version v1.0
 * @since v7.1.5
 * 2019-10-28
 */
@Schema
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class AfterSaleGoodsVO implements Serializable {

    private static final long serialVersionUID = -4144278368570923608L;

    /**
     * 商品ID
     */
    @Schema(name = "goods_id", description = "商品ID")
    private Long goodsId;
    /**
     * 商品SKUID
     */
    @Schema(name = "sku_id", description = "商品SKUID")
    private Long skuId;
    /**
     * 商品成交价
     */
    @Schema(name = "price", description = "商品成交价")
    private Double price;
    /**
     * 购买数量
     */
    @Schema(name = "ship_num", description = "购买数量")
    private Integer shipNum;
    /**
     * 退还数量
     */
    @Schema(name = "return_num", description = "退还数量")
    private Integer returnNum;
    /**
     * 商品名称
     */
    @Schema(name = "goods_name", description = "商品名称")
    private String goodsName;
    /**
     * 商品缩略图
     */
    @Schema(name = "goods_image", description = "商品缩略图")
    private String goodsImage;

    public Long getGoodsId() {
        return goodsId;
    }

    public void setGoodsId(Long goodsId) {
        this.goodsId = goodsId;
    }

    public Long getSkuId() {
        return skuId;
    }

    public void setSkuId(Long skuId) {
        this.skuId = skuId;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Integer getShipNum() {
        return shipNum;
    }

    public void setShipNum(Integer shipNum) {
        this.shipNum = shipNum;
    }

    public Integer getReturnNum() {
        return returnNum;
    }

    public void setReturnNum(Integer returnNum) {
        this.returnNum = returnNum;
    }

    public String getGoodsName() {
        return goodsName;
    }

    public void setGoodsName(String goodsName) {
        this.goodsName = goodsName;
    }

    public String getGoodsImage() {
        return goodsImage;
    }

    public void setGoodsImage(String goodsImage) {
        this.goodsImage = goodsImage;
    }

    @Override
    public String toString() {
        return "AfterSaleGoodsVO{" +
                "goodsId=" + goodsId +
                ", skuId=" + skuId +
                ", price=" + price +
                ", shipNum=" + shipNum +
                ", returnNum=" + returnNum +
                ", goodsName='" + goodsName + '\'' +
                ", goodsImage='" + goodsImage + '\'' +
                '}';
    }
}
