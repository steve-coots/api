/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.security;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

import java.util.Objects;

/**
 * 篡改记录
 * @author 妙贤
 * @version 1.0
 * @data 2021/11/20 10:57
 **/
@TableName("es_falsify_record")
public class FalsifyRecord {

    /**
     * 记录id
     */
    @TableId(type= IdType.ASSIGN_ID)
    private Long recordId;

    /**
     * 篡改模块
     */
    private String module;

    /**
     * 模块名称
     */
    private String moduleName;

    /**
     * 被篡改的数据id
     */
    private Long dataId;

    /**
     * 状态
     */
    private String state;

    /**
     * 发现时间（扫描发现的时刻）
     */
    private Long sanTime;

    public Long getRecordId() {
        return recordId;
    }

    public void setRecordId(Long recordId) {
        this.recordId = recordId;
    }

    public String getModule() {
        return module;
    }

    public void setModule(String module) {
        this.module = module;
    }

    public Long getDataId() {
        return dataId;
    }

    public void setDataId(Long dataId) {
        this.dataId = dataId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Long getSanTime() {
        return sanTime;
    }

    public void setSanTime(Long sanTime) {
        this.sanTime = sanTime;
    }

    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String moduleName) {
        this.moduleName = moduleName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        FalsifyRecord that = (FalsifyRecord) o;
        return Objects.equals(recordId, that.recordId) && Objects.equals(module, that.module) && Objects.equals(dataId, that.dataId) && Objects.equals(state, that.state);
    }

    @Override
    public int hashCode() {
        return Objects.hash(recordId, module, dataId, state);
    }

    @Override
    public String toString() {
        return "FalsifyRecord{" +
                "recordId=" + recordId +
                ", module='" + module + '\'' +
                ", dataId=" + dataId +
                ", state='" + state + '\'' +
                '}';
    }
}
