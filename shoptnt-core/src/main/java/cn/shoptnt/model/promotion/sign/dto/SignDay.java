package cn.shoptnt.model.promotion.sign.dto;

import io.swagger.v3.oas.annotations.media.Schema;

import java.io.Serializable;

/**
 * @author zh
 * @version 1.0
 * @title SignDay
 * @description 签到详细信息
 * @program: api
 * 2024/3/13 15:00
 */
public class SignDay implements Serializable {
    private static final long serialVersionUID = 402464628793005588L;

    @Schema(description =  "本月签到天")
    private Integer signDay;
    @Schema(description =  "是否签到")
    private Boolean sign;
    @Schema(description =  "是否连续签到")
    private Boolean signPrize;
    @Schema(description =  "日期")
     private String day;


    public Integer getSignDay() {
        return signDay;
    }

    public void setSignDay(Integer signDay) {
        this.signDay = signDay;
    }

    public Boolean getSign() {
        return sign;
    }

    public void setSign(Boolean sign) {
        this.sign = sign;
    }

    public Boolean getSignPrize() {
        return signPrize;
    }

    public void setSignPrize(Boolean signPrize) {
        this.signPrize = signPrize;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    @Override
    public String toString() {
        return "SignDay{" +
                "signDay=" + signDay +
                ", sign=" + sign +
                ", signPrize=" + signPrize +
                ", day='" + day + '\'' +
                '}';
    }
}
