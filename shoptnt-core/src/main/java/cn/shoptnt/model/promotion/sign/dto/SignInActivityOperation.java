package cn.shoptnt.model.promotion.sign.dto;

import cn.shoptnt.model.promotion.sign.dos.SignInReward;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.List;

/**
 * @author zh
 * @version 1.0
 * @title SignInActiveAdd
 * @description 签到活动添加
 * @program: api
 * 2024/3/12 17:39
 */
public class SignInActivityOperation implements Serializable {
    private static final long serialVersionUID = -606948710004977601L;
    @NotBlank(message = "活动标题不能为空")
    @Schema(description =  "活动标题", required = true)
    private String title;
    @NotNull(message = "开始时间不能为空")
    @Schema(description =  "开始日期", required = true)
    private Long startDate;
    @NotNull(message = "开始时间不能为空")
    @Schema(description =  "结束日期", required = true)
    private Long endDate;
    @NotBlank(message = "活动说明不能为空")
    @Schema(description =  "活动说明", required = true)
    private String description;

    @Schema(description =  "推荐产品ID")
    private List<Long> products;
    @Schema(description =  "推荐产品ID", required = true)
    private List<SignInReward> rewards;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getStartDate() {
        return startDate;
    }

    public void setStartDate(Long startDate) {
        this.startDate = startDate;
    }

    public Long getEndDate() {
        return endDate;
    }

    public void setEndDate(Long endDate) {
        this.endDate = endDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Long> getProducts() {
        return products;
    }

    public void setProducts(List<Long> products) {
        this.products = products;
    }

    public List<SignInReward> getRewards() {
        return rewards;
    }

    public void setRewards(List<SignInReward> rewards) {
        this.rewards = rewards;
    }


    @Override
    public String toString() {
        return "SignInActiveAdd{" +
                "title='" + title + '\'' +
                ", startDate=" + startDate +
                ", endDate=" + endDate +
                ", description='" + description + '\'' +
                ", products=" + products +
                ", rewards=" + rewards +
                '}';
    }
}
