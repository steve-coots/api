/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.promotion.pintuan;

import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * Created by 妙贤 on 2019-01-24.
 * 参团者
 * @author 妙贤
 * @version 1.0
 * @since 7.1.0
 * 2019-01-24
 */
public class Participant {

    public Participant() {
        isMaster=0;
    }

    @Schema(name = "id", description =  "会员id" )
    private Long id;

    @Schema(name = "name", description =  "会员名" )
    private String name;

    @Schema(name = "face", description =  "头像" )
    private String face;

    @Schema(name = "is_master", description =  "是否是团长,1是，0否" )
    private Integer isMaster;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFace() {
        return face;
    }

    public void setFace(String face) {
        this.face = face;
    }

    public Integer getIsMaster() {
        return isMaster;
    }

    public void setIsMaster(Integer isMaster) {
        this.isMaster = isMaster;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Participant that = (Participant) o;

        return new EqualsBuilder()
                .append(getId(), that.getId())
                .append(getName(), that.getName())
                .append(getFace(), that.getFace())
                .append(getIsMaster(), that.getIsMaster())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(getId())
                .append(getName())
                .append(getFace())
                .append(getIsMaster())
                .toHashCode();
    }

    @Override
    public String toString() {
        return "Participant{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", face='" + face + '\'' +
                ", isMaster=" + isMaster +
                '}';
    }
}
