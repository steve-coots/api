/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.support.validator.annotation;
/**
 * 日志级别
 * @author 妙贤
 * @data 2021/11/24 14:21
 * @version 1.0
 **/
public enum LogLevel {
    /**
     * 一般的、日常级别
     */
    normal,
    /**
     * 重要的级别
     */
    important
}
