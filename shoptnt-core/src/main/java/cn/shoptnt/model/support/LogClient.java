/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.support;

/**
 * 日志级别
 * @author fk
 * @data 2021年11月29日11:44:22
 * @version 1.0
 **/
public enum LogClient {

    /**
     * 管理端
     */
    admin,

    /**
     * 商家端
     */
    seller;


}
