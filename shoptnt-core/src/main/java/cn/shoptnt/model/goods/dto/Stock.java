package cn.shoptnt.model.goods.dto;

/**
 * 库存对象模型
 *
 * @author kingapex
 * @version 1.0
 * @data 2022/10/21 19:28
 **/
public class Stock {

    /**
     * 实际库存
     */
    private Integer actual;

    /**
     * 可用库存
     */
    private Integer enable;

    /**
     * skuId或goodsId
     */
    private Long stockSn;

    public Stock(Long stockSn, Integer enable, Integer actual) {
        this.enable = enable;
        this.actual = actual;
        this.stockSn = stockSn;
    }

    public Integer getActual() {
        return actual;
    }

    public Integer getEnable() {
        return enable;
    }

    public Long getStockSn() {
        return stockSn;
    }

    public void setStockSn(Long stockSn) {
        this.stockSn = stockSn;
    }

    @Override
    public String toString() {
        return "Stock{" +
                "actual=" + actual +
                ", enable=" + enable +
                ", stockSn=" + stockSn +
                '}';
    }
}
