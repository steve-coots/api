/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.shop.vo;

import io.swagger.v3.oas.annotations.media.Schema;

import java.util.List;


/**
 * 菜单管理店铺实体
 *
 * @author zh
 * @version v7.0.0
 * @since v7.0.0
 * 2018-08-02 15:38:40
 */
public class ShopMenus {

    /**
     * 菜单标题
     */
    @Schema(description =  "菜单标题")
    private String title;
    /**
     * 菜单唯一标识
     */
    @Schema(description =  "菜单唯一标识")
    private String identifier;
    /**
     * 此菜单是否选中
     */
    @Schema(description =  "此菜单是否选中")
    private boolean checked;
    /**
     * 菜单的权限表达式
     */
    @Schema(description =  "菜单的权限表达式")
    private String authRegular;
    /**
     * 子菜单
     */
    @Schema(description =  "子菜单")
    private List<ShopMenus> children;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIdentifier() {
        return identifier;
    }

    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getAuthRegular() {
        return authRegular;
    }

    public void setAuthRegular(String authRegular) {
        this.authRegular = authRegular;
    }

    public List<ShopMenus> getChildren() {
        return children;
    }

    public void setChildren(List<ShopMenus> children) {
        this.children = children;
    }

    @Override
    public String toString() {
        return "ShopMenus{" +
                "title='" + title + '\'' +
                ", identifier='" + identifier + '\'' +
                ", checked=" + checked +
                ", authRegular='" + authRegular + '\'' +
                ", children=" + children +
                '}';
    }
}