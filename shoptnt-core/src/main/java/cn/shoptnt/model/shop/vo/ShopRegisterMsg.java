package cn.shoptnt.model.shop.vo;

import cn.shoptnt.framework.message.direct.AmqpExchange;
import cn.shoptnt.framework.message.direct.DirectMessage;

import java.io.Serializable;
import java.util.Objects;

/**
 * 商家入驻消息实体
 * @version 1.0
 * @since 5.2.3
 * 2022-05-07
 */
public class ShopRegisterMsg implements Serializable, DirectMessage {

    private static final long serialVersionUID = -1666685960220713634L;

    /** 商家ID */
    private Long sellerId;

    /** 操作类型 */
    private Integer operateType;

    /**
     * 申请入驻
     */
    public final static int APPLY = 1;

    /**
     * 审核通过
     */
    public final static int PASS = 2;

    /**
     * 审核未通过
     */
    public final static int REFUSE = 3;

    public ShopRegisterMsg() {

    }

    public ShopRegisterMsg(Long sellerId, Integer operateType) {
        this.sellerId = sellerId;
        this.operateType = operateType;
    }

    public Long getSellerId() {
        return sellerId;
    }

    public void setSellerId(Long sellerId) {
        this.sellerId = sellerId;
    }

    public Integer getOperateType() {
        return operateType;
    }

    public void setOperateType(Integer operateType) {
        this.operateType = operateType;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        ShopRegisterMsg that = (ShopRegisterMsg) o;
        return Objects.equals(sellerId, that.sellerId) &&
                Objects.equals(operateType, that.operateType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(sellerId, operateType);
    }

    @Override
    public String toString() {
        return "ShopRegisterMsg{" +
                "sellerId=" + sellerId +
                ", operateType=" + operateType +
                '}';
    }

    @Override
    public String getExchange() {
        return AmqpExchange.SHOP_REGISTER;
    }
}
