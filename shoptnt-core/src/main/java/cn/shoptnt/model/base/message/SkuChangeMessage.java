package cn.shoptnt.model.base.message;


import cn.shoptnt.framework.message.direct.AmqpExchange;
import cn.shoptnt.framework.message.direct.DirectMessage;

import java.io.Serializable;
import java.util.List;

/**
 * sku变更消息
 * @author zs
 * @since 2024-03-08
 **/
public class SkuChangeMessage implements DirectMessage, Serializable {


    private static final long serialVersionUID = -977600762323161940L;

    /**
     * 变更资源，sku id集合
     */
    private List<Long> skuIds;


    /**
     * 操作类型
     */
    private Integer operationType;

    /**
     * 删除
     */
    public final static int DEL_OPERATION = 3;

    public SkuChangeMessage(List<Long> skuIds) {
        this.skuIds = skuIds;
    }

    @Override
    public String getExchange() {
        return  AmqpExchange.GOODS_SKU_CHANGE;
    }

    public List<Long> getSkuIds() {
        return skuIds;
    }
}
