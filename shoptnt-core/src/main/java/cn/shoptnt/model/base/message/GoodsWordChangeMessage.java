package cn.shoptnt.model.base.message;


import cn.shoptnt.framework.message.direct.AmqpExchange;
import cn.shoptnt.framework.message.direct.DirectMessage;

import java.io.Serializable;

/**
 * 分词变化消息
 * @author zs
 * @since 2024-03-08
 **/
public class GoodsWordChangeMessage implements Serializable, DirectMessage {

    private static final long serialVersionUID = -6042346606426853823L;

    private String word;

    public GoodsWordChangeMessage(String word) {
        this.word = word;
    }

    public String getWord() {
        return word;
    }

    @Override
    public String getExchange() {
        return AmqpExchange.GOODS_WORDS_CHANGE;
    }
}
