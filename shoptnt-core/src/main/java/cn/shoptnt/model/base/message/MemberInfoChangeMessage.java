package cn.shoptnt.model.base.message;


import cn.shoptnt.framework.message.direct.AmqpExchange;
import cn.shoptnt.framework.message.direct.DirectMessage;

import java.io.Serializable;

/**
 * 会员信息变化消息
 *
 * @author zs
 * @since 2022/10/20 15:45
 **/
public class MemberInfoChangeMessage implements Serializable, DirectMessage {

    private Long memberId;

    private static final long serialVersionUID = -8761441921918746501L;

    public MemberInfoChangeMessage(Long memberId) {
        this.memberId = memberId;
    }

    @Override
    public String getExchange() {
        return AmqpExchange.MEMBER_INFO_CHANGE;
    }

    public Long getMemberId() {
        return memberId;
    }


}
