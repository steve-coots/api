/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.base;


import java.lang.annotation.*;

/**
 * 地区注解
 *
 * @author zh
 * @version v7.0
 * @date 18/4/27 下午3:51
 * @since v7.0
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Region {

}
