/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.pagedata.enums;

/**
 * @Author shen
 * @Date 2021/3/15 9:36
 * 微页面发布状态状态和是否是首页
 */
public enum PageStatus {
    /**
	 *发布状态为是  或者 是首页
	 */
	YES,
    /**
     *发布状态为否  或者 不是首页
     */
    NO;


	PageStatus() {

	}

	public String value() {
		return this.name();
	}
}
