/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.model.trade.order.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.v3.oas.annotations.media.Schema;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;

/**
 * 付款单查询参数
 *
 * @author Snow create in 2018/7/18
 * @version v2.0
 * @since v7.0.0
 */
@JsonNaming(value = PropertyNamingStrategy.SnakeCaseStrategy.class)
public class PayLogQueryParam {

    @Schema(name = "page_no",description = "第几页")
    private Long pageNo;

    @Schema(name = "page_size",description = "每页条数")
    private Long pageSize;

    @Schema(name = "pay_way",description = "支付方式")
    private String payWay;

    @Schema(name = "start_time",description = "开始时间")
    private Long startTime;

    @Schema(name = "end_time",description = "截止时间")
    private Long endTime;

    @Schema(name = "order_sn",description = "订单编号")
    private String orderSn;

    @Schema(name = "member_name",description = "付款会员名")
    private String memberName;

    @Schema(name = "payment_type",description = "付款方式",allowableValues = "ONLINE,COD")
    private String paymentType;

    @Schema(name = "pay_status",description = "支付状态",allowableValues = "PAY_YES,PAY_NO")
    private String payStatus;


    public Long getPageNo() {
        return pageNo;
    }

    public void setPageNo(Long pageNo) {
        this.pageNo = pageNo;
    }

    public Long getPageSize() {
        return pageSize;
    }

    public void setPageSize(Long pageSize) {
        this.pageSize = pageSize;
    }

    public String getPayWay() {
        return payWay;
    }

    public void setPayWay(String payWay) {
        this.payWay = payWay;
    }

    public Long getStartTime() {
        return startTime;
    }

    public void setStartTime(Long startTime) {
        this.startTime = startTime;
    }

    public Long getEndTime() {
        return endTime;
    }

    public void setEndTime(Long endTime) {
        this.endTime = endTime;
    }

    public String getOrderSn() {
        return orderSn;
    }

    public void setOrderSn(String orderSn) {
        this.orderSn = orderSn;
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

    public String getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(String paymentType) {
        this.paymentType = paymentType;
    }

    public String getPayStatus() {
        return payStatus;
    }

    public void setPayStatus(String payStatus) {
        this.payStatus = payStatus;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o){
            return true;
        }

        if (o == null || getClass() != o.getClass()){
            return false;
        }

        PayLogQueryParam that = (PayLogQueryParam) o;

        return new EqualsBuilder()
                .append(pageNo, that.pageNo)
                .append(pageSize, that.pageSize)
                .append(payWay, that.payWay)
                .append(startTime, that.startTime)
                .append(endTime, that.endTime)
                .append(orderSn, that.orderSn)
                .append(memberName, that.memberName)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37)
                .append(pageNo)
                .append(pageSize)
                .append(payWay)
                .append(startTime)
                .append(endTime)
                .append(orderSn)
                .append(memberName)
                .toHashCode();
    }

    @Override
    public String toString() {
        return "PayLogQueryParam{" +
                "pageNo=" + pageNo +
                ", pageSize=" + pageSize +
                ", payWay='" + payWay + '\'' +
                ", startTime=" + startTime +
                ", endTime=" + endTime +
                ", orderSn='" + orderSn + '\'' +
                ", memberName='" + memberName + '\'' +
                '}';
    }
}
