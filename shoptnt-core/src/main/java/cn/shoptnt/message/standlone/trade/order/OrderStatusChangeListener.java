package cn.shoptnt.message.standlone.trade.order;

import cn.shoptnt.message.dispatcher.trade.order.OrderStatusChangeDispatcher;
import cn.shoptnt.model.base.message.OrderStatusChangeMsg;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionalEventListener;

/**
 * 订单状态改变 监听器
 * 对ApplicationEventPublisher的publishEvent的监听，默认在事务提交后执行
 *
 * @author LiuXT
 * @date 2022-10-21
 * @since v7.3.0
 */
@Component
@ConditionalOnProperty(value = "shoptnt.runmode", havingValue = "standalone")
public class OrderStatusChangeListener {

    @Autowired
    private OrderStatusChangeDispatcher orderStatusChangeDispatcher;

    @TransactionalEventListener(fallbackExecution = true)
    @Async
    public void orderChange(OrderStatusChangeMsg orderMessage) throws InterruptedException {
        orderStatusChangeDispatcher.dispatch(orderMessage);
    }
}
