/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.message.delay;

import cn.shoptnt.client.promotion.PromotionGoodsClient;
import cn.shoptnt.client.promotion.PromotionScriptClient;
import cn.shoptnt.model.base.message.PromotionScriptMsg;
import cn.shoptnt.framework.message.TimeExecute;
import cn.shoptnt.model.promotion.tool.dos.PromotionGoodsDO;
import cn.shoptnt.model.promotion.tool.enums.PromotionTypeEnum;
import cn.shoptnt.model.promotion.tool.enums.ScriptOperationTypeEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import cn.shoptnt.framework.trigger.Interface.TimeTrigger;
import cn.shoptnt.framework.trigger.Interface.TimeTriggerExecuter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 团购促销活动脚本生成和删除延时任务执行器
 * @author duanmingyu
 * @version v1.0
 * @since v7.2.0
 * 2020-02-18
 */
@Component("groupBuyScriptTimeTriggerExecuter")
public class GroupBuyScriptTimeTriggerExecuter implements TimeTriggerExecuter {

    protected final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private TimeTrigger timeTrigger;

    @Autowired
    private PromotionScriptClient promotionScriptClient;

    @Autowired
    private PromotionGoodsClient promotionGoodsClient;

    @Override
    public void execute(Object object) {

        PromotionScriptMsg promotionScriptMsg = (PromotionScriptMsg) object;

        //获取促销活动ID
        Long promotionId = promotionScriptMsg.getPromotionId();

        //获取参与团购活动的所有商品信息集合
        List<PromotionGoodsDO> goodsList = this.promotionGoodsClient.getPromotionGoods(promotionId, PromotionTypeEnum.GROUPBUY.name());

        //如果是团购促销活动开始生效
        if (ScriptOperationTypeEnum.CREATE.equals(promotionScriptMsg.getOperationType())) {
            //创建脚本信息
            this.promotionScriptClient.createGroupBuyCacheScript(promotionId, goodsList);

            //开启活动后，立马设置一个关闭的流程
            promotionScriptMsg.setOperationType(ScriptOperationTypeEnum.DELETE);

            String uniqueKey = "{TIME_TRIGGER_" + promotionScriptMsg.getPromotionType().name() + "}_" + promotionId;

            timeTrigger.add(TimeExecute.GROUPBUY_SCRIPT_EXECUTER, promotionScriptMsg, promotionScriptMsg.getEndTime(), uniqueKey);

            this.logger.debug("促销活动[" + promotionScriptMsg.getPromotionName() + "]开始，id=[" + promotionId + "]");

        } else {
            //删除脚本信息
            this.promotionScriptClient.deleteGroupBuyCacheScript(promotionId, goodsList);

            this.logger.debug("促销活动[" + promotionScriptMsg.getPromotionName() + "]结束，id=[" + promotionId + "]");
        }

    }
}
