package cn.shoptnt.message.dispatcher.goods;

import cn.shoptnt.message.event.CategoryChangeEvent;
import cn.shoptnt.model.base.message.CategoryChangeMsg;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 分类变更 调度器
 *
 * @author LiuXT
 * @date 2022-10-21
 * @since v7.3.0
 */
@Service
public class CategoryChangeDispatcher {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = false)
    private List<CategoryChangeEvent> events;

    public void dispatch(CategoryChangeMsg categoryChangeMsg) {

        if (events != null) {
            for (CategoryChangeEvent event : events) {
                try {
                    event.categoryChange(categoryChangeMsg);
                } catch (Exception e) {
                    logger.error("处理商品分类变化消息出错", e);
                }
            }
        }
    }
}
