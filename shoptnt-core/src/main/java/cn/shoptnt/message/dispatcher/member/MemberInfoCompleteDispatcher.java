package cn.shoptnt.message.dispatcher.member;

import cn.shoptnt.message.event.MemberInfoCompleteEvent;
import cn.shoptnt.model.base.message.MemberInfoCompleteMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 会员完善个人信息 调度器
 *
 * @author LiuXT
 * @date 2022-10-21
 * @since v7.3.0
 */
@Service
public class MemberInfoCompleteDispatcher {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired(required = false)
    private List<MemberInfoCompleteEvent> events;

    public void dispatch(MemberInfoCompleteMessage message) {
        if (events != null) {
            for (MemberInfoCompleteEvent event : events) {
                try {
                    event.memberInfoComplete(message.getMemberId());
                } catch (Exception e) {
                    logger.error("会员完善信息出错", e);
                }
            }
        }
    }
}
