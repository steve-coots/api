/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.message.event;

import cn.shoptnt.model.base.message.MemberAskMessage;

/**
 * 会员商品咨询事件
 *
 * @author duanmingyu
 * @version v1.0
 * @since v7.1.5
 * 2019-09-16
 */
public interface MemberAskSendMessageEvent {

    /**
     * 会员商品咨询后执行
     *
     * @param memberAskMessage
     */
    void goodsAsk(MemberAskMessage memberAskMessage);
}
