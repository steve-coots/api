package cn.shoptnt.message.consumer.promotion;

import cn.shoptnt.client.promotion.SignInActivityClient;
import cn.shoptnt.message.event.MemberSignEvent;
import cn.shoptnt.model.promotion.sign.dos.SignInRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author zh
 * @version 1.0
 * @title SignConsumer
 * @description <TODO description class>
 * @program: api
 * 2024/3/19 11:19
 */
@Component
public class SignConsumer implements MemberSignEvent {


    @Autowired
    private SignInActivityClient signInActivityClient;

    @Override
    public void sign(List<SignInRecord> signInRecords) {
        signInActivityClient.sendGift(signInRecords);
    }
}
