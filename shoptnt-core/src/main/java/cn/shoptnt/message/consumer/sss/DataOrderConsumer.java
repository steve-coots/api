/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.message.consumer.sss;

import cn.shoptnt.message.event.ASNewOrderEvent;
import cn.shoptnt.message.event.OrderStatusChangeEvent;
import cn.shoptnt.framework.util.LogUtils;
import cn.shoptnt.model.base.message.OrderStatusChangeMsg;
import cn.shoptnt.client.statistics.OrderDataClient;
import cn.shoptnt.model.trade.order.enums.OrderStatusEnum;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 订单状态改变消费
 *
 * @author chopper
 * @version v1.0
 * @Description:
 * @since v7.0
 * 2018/5/8 下午6:44
 */
@Component
public class DataOrderConsumer implements OrderStatusChangeEvent {

    private Logger logger = LoggerFactory.getLogger(getClass());


    @Autowired
    private OrderDataClient orderDataClient;

    @Override
    public void orderChange(OrderStatusChangeMsg orderStatusChangeMsg) {


        try {
            if (orderStatusChangeMsg.getNewStatus().equals(OrderStatusEnum.PAID_OFF)) {
                LogUtils.consumerInfo("支付成功创建统计订单记录" + orderStatusChangeMsg.getOrderDO().getSn());
                //已付款订单
                this.orderDataClient.put(orderStatusChangeMsg.getOrderDO());
            } else {
                LogUtils.consumerInfo("修改统计订单记录" + orderStatusChangeMsg.getOrderDO().getSn());
                this.orderDataClient.change(orderStatusChangeMsg.getOrderDO());
            }
        } catch (Exception e) {
            LogUtils.sendInfo("订单记录异常" + orderStatusChangeMsg.getOrderDO().getSn() + e.getMessage());
            logger.error("订单变更消息异常:", e);
            e.printStackTrace();
        }
    }

}
