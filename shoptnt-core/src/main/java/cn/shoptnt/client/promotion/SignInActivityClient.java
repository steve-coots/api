package cn.shoptnt.client.promotion;

import cn.shoptnt.model.promotion.sign.dos.SignInRecord;

import java.util.List;

/**
 * @author zh
 * @version 1.0
 * @title SignInActivityClient
 * @description 签到客户端
 * @program: api
 * 2024/3/19 11:45
 */
public interface SignInActivityClient {
    /**
     * 根据签到记录赠送礼物
     *
     * @param recordList 签到记录
     */
    void sendGift(List<SignInRecord> recordList);
}
