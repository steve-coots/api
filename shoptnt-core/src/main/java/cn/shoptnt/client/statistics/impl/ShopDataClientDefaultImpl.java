/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.statistics.impl;

import cn.shoptnt.client.statistics.ShopDataClient;
import cn.shoptnt.model.statistics.dto.ShopData;
import cn.shoptnt.service.statistics.ShopDataManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

/**
 * ShopDataClientDefaultImpl
 *
 * @author Chopper
 * @version v1.0
 * @since v7.0
 * 2018-08-14 下午2:44
 */
@Service
@ConditionalOnProperty(value="shoptnt.product", havingValue="stand")
public class ShopDataClientDefaultImpl implements ShopDataClient {
    @Autowired
    private ShopDataManager shopDataManager;
    /**
     * 修改店铺收藏数量
     *
     * @param shopData
     */
    @Override
    public void updateCollection(ShopData shopData) {
        shopDataManager.updateCollection(shopData);
    }

    @Override
    public void updateShopData(ShopData shopData) {
        shopDataManager.updateShopData(shopData);
    }

    /**
     * 添加店铺收藏数量
     *
     * @param shopData
     */
    @Override
    public void add(ShopData shopData) {
        shopDataManager.add(shopData);
    }
}
