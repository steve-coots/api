/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.system.impl;

import cn.shoptnt.client.system.PageDataClient;
import cn.shoptnt.mapper.system.PageDataGoodsMapper;
import cn.shoptnt.model.pagedata.PageData;
import cn.shoptnt.service.pagedata.PageDataManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 楼层渲染实现
 *
 * @author zh
 * @version v1.0
 * @since v7.0
 * 2018-08-14 下午2:37
 */
@Service
@ConditionalOnProperty(value = "shoptnt.product", havingValue = "stand")
public class PageDataClientDefaultImpl implements PageDataClient {

    @Autowired
    private PageDataManager pageDataManager;

    @Autowired
    private PageDataGoodsMapper pageDataGoodsMapper;


    @Override
    public PageData edit(PageData page, Long id) {
        return pageDataManager.edit(page,id);
    }

    @Override
    public List<PageData> selectPageByGoods(Long[] goodsIds) {

        return pageDataGoodsMapper.selectPageList(goodsIds);
    }
}
