/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.system.impl;

import cn.shoptnt.model.base.SceneType;
import cn.shoptnt.model.base.vo.EmailVO;
import cn.shoptnt.service.base.service.EmailManager;
import cn.shoptnt.client.system.EmailClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

/**
 * @author fk
 * @version v2.0
 * @Description:
 * @date 2018/8/13 16:26
 * @since v7.0.0
 */
@Service
@ConditionalOnProperty(value="shoptnt.product", havingValue="stand")
public class EmailClientDefaultImpl implements EmailClient {

    @Autowired
    private EmailManager emailManager;

    @Override
    public void sendEmail(EmailVO emailVO) {
        emailManager.sendEmail(emailVO);
    }

    @Override
    public boolean valid(String scene, String email, String code) {
        return emailManager.valid(scene, email, code);
    }

    @Override
    public void sendEmailMessage(String byName, String email, SceneType sceneType) {
        emailManager.sendEmailMessage(byName, email, sceneType);
    }
}
