/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.client.distribution;

import cn.shoptnt.model.distribution.dos.CommissionTpl;

/**
 * 模版client
 * @author liushuai
 * @version v1.0
 * @since v7.0
 * 2018/8/14 下午2:17
 * @Description:
 *
 */ 

public interface CommissionTplClient {

    /**
     * 获取默认模版
     * @return
     */
    CommissionTpl getDefaultCommission();

}
