package cn.shoptnt.service.trade.order;

import cn.shoptnt.model.trade.order.vo.TradeVO;

/**
 * @author gy
 * @version 1.0
 * @sinc 7.x
 * @date 2023年01月04日 11:00
 */
public interface PlatformCouponUseManger {


  /**
   * 使用平台优惠券
   * @param tradeVO 交易VO
   */
  void  useCoupon(TradeVO tradeVO);
}
