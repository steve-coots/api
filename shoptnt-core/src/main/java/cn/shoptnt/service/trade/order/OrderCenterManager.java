/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.trade.order;

import cn.shoptnt.model.trade.order.dto.OrderDTO;
import cn.shoptnt.model.trade.order.vo.OrderParam;

/**
 *
 * @description: 订单中心业务
 * @author: liuyulei
 * @create: 2020/3/23 18:39
 * @version:1.0
 * @since:
 **/
public interface OrderCenterManager {


    /**
     * 创建订单对象
     * @param orderParam  订单参数
     * @return 订单DTO
     */
    OrderDTO createOrder(OrderParam orderParam);




}
