package cn.shoptnt.service.promotion.sign;

import cn.shoptnt.model.promotion.sign.dos.SignInReward;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * @author zh
 * @version 1.0
 * @title signInRewardManager
 * @description 签到奖品业务类
 * @program: api
 * 2024/3/12 12:05
 */
public interface SignInRewardManager extends IService<SignInReward> {

    /**
     * 根据活动id查询奖品设置信息
     *
     * @param activityId 签到活动
     * @return
     */
    List<SignInReward> getByActivityId(Long activityId);

    /**
     * 根据活动id删除设置
     * @param activityId 活动id
     */
    void deleteByActivityId(Long activityId);
}
