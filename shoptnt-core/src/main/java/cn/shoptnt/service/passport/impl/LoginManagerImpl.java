/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.passport.impl;

import cn.hutool.core.lang.UUID;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import cn.shoptnt.framework.auth.Token;
import cn.shoptnt.framework.cache.Cache;
import cn.shoptnt.framework.context.request.ThreadContextHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import cn.shoptnt.framework.message.direct.DirectMessageSender;
import cn.shoptnt.framework.security.TokenManager;
import cn.shoptnt.framework.security.model.Buyer;
import cn.shoptnt.framework.util.DateUtil;
import cn.shoptnt.framework.util.EmojiCharacterUtil;
import cn.shoptnt.framework.util.StringUtil;
import cn.shoptnt.mapper.member.ConnectMapper;
import cn.shoptnt.mapper.member.MemberMapper;
import cn.shoptnt.model.base.CachePrefix;
import cn.shoptnt.model.base.message.MemberRegisterMsg;
import cn.shoptnt.framework.message.direct.AmqpExchange;
import cn.shoptnt.model.member.dos.ConnectDO;
import cn.shoptnt.model.member.dos.Member;
import cn.shoptnt.model.member.dto.LoginUserDTO;
import cn.shoptnt.model.member.enums.ConnectTypeEnum;
import cn.shoptnt.model.member.vo.Auth2Token;
import cn.shoptnt.model.member.vo.MemberLoginMsg;
import cn.shoptnt.model.member.vo.MemberVO;
import cn.shoptnt.service.member.MemberManager;
import cn.shoptnt.service.passport.LoginManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

/**
 * 三方登陆服务
 *
 * @author cs
 * 2020/11/02
 */
@Service
public class LoginManagerImpl implements LoginManager {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private ConnectMapper connectMapper;

    @Autowired
    private MemberManager memberManager;

    @Autowired
    private MemberMapper memberMapper;

    @Autowired
    private Cache cache;

    @Autowired
    private DirectMessageSender messageSender;

    @Autowired
    private TokenManager tokenManager;


    /**
     * 根据UnionId登陆
     *
     * @param loginUserDTO
     * @return
     */
    @Override
    public Map loginByUnionId(LoginUserDTO loginUserDTO) {
        Map res = new HashMap(16);
        //通过unionid查找会员(es_connect表)
        ConnectDO connectDO = findConnectByUnionId(loginUserDTO.getUnionid(), loginUserDTO.getUnionType());
        Member member = null;
        if (connectDO == null) {
            //没找到注册一个
            member = register(loginUserDTO);
        } else {
            member = findMemberById(connectDO.getMemberId());
            //查看当前登陆终端该账户openid是否已记录，如果未记录则新增记录
            if (loginUserDTO.getOpenType() != null) {
                ConnectDO aDo = connectMapper.selectOne(new QueryWrapper<ConnectDO>().eq("member_id", member.getMemberId()).eq("union_type", loginUserDTO.getOpenType().value()));
                if (aDo == null) {
                    ConnectDO connect = new ConnectDO();
                    connect.setMemberId(member.getMemberId());
                    connect.setUnionId(loginUserDTO.getOpenid());
                    connect.setUnionType(loginUserDTO.getOpenType().value());
                    connectMapper.insert(connect);
                } else if (StringUtil.isEmpty(aDo.getUnionId())) {
                    aDo.setUnionId(loginUserDTO.getOpenid());
                    aDo.setUnboundTime(null);
                    connectMapper.updateById(aDo);
                }
            }
        }
        //存储uuid和unionId的关系
        Auth2Token auth2Token = new Auth2Token();
        if (null == loginUserDTO.getOpenType()) {
            auth2Token.setType(loginUserDTO.getUnionType().value());
        } else {
            auth2Token.setType(loginUserDTO.getOpenType().value());
        }
        auth2Token.setUnionid(loginUserDTO.getUnionid());
        if (!StringUtil.isEmpty(loginUserDTO.getOpenid())) {
            auth2Token.setOpneId(loginUserDTO.getOpenid());
        }
        logger.debug("三方登录openId为：" + loginUserDTO.getOpenid() + ";unionid为" + loginUserDTO.getUnionid());
        cache.put(CachePrefix.CONNECT_LOGIN.getPrefix() + loginUserDTO.getUuid(), auth2Token);
        MemberVO memberVO = this.connectWeChatLoginHandle(member, loginUserDTO.getUuid(), loginUserDTO.getTokenOutTime(), loginUserDTO.getRefreshTokenOutTime());
        res.put("access_token", memberVO.getAccessToken());
        res.put("refresh_token", memberVO.getRefreshToken());
        res.put("uid", memberVO.getUid());
        return res;
    }

    /**
     * 根据unionId查询connect
     *
     * @param unionId
     * @return
     */
    private ConnectDO findConnectByUnionId(String unionId, ConnectTypeEnum unionType) {
        return connectMapper.selectOne(new QueryWrapper<ConnectDO>().eq("union_id", unionId).eq("union_type", unionType.value()).isNull("unbound_time"));
    }

    /**
     * 注册
     *
     * @param loginUserDTO
     * @return
     */
    private Member register(LoginUserDTO loginUserDTO) {
        String usernamePrefix = "m_";
        String usernameSuffix = UUID.fastUUID().toString(true).substring(0, 9);
        String username = usernamePrefix + usernameSuffix;
        Member memberByName = memberManager.getMemberByName(username);
        while (memberByName != null) {
            usernameSuffix = UUID.fastUUID().toString(true).substring(0, 9);
            username = usernamePrefix + usernameSuffix;
            memberByName = memberManager.getMemberByName(username);
        }
        Member member = new Member();
        member.setUname(username);
        member.setSex(loginUserDTO.getSex());
        long dateline = DateUtil.getDateline();
        member.setCreateTime(dateline);
        member.setFace(loginUserDTO.getHeadimgurl());
        member.setRegisterIp(ThreadContextHolder.getHttpRequest().getRemoteAddr());
        member.setLoginCount(1);
        member.setLastLogin(dateline);
        member.setDisabled(0);
        String nickname = loginUserDTO.getNickName();
        nickname = StringUtil.isEmpty(nickname) ? username : EmojiCharacterUtil.encode(nickname);
        member.setNickname(nickname);
        member.setProvince(loginUserDTO.getProvince());
        member.setCity(loginUserDTO.getCity());
        //设置会员等级积分为0
        member.setGradePoint(0L);
        //设置会员消费积分为0
        member.setConsumPoint(0L);
        //设置会员是否开启店铺 0：否，1：是
        member.setHaveShop(0);
        //设置会员是否完善了个人信息 0：否，1：是
        member.setInfoFull(0);
        memberMapper.insert(member);
        addConnect(loginUserDTO, member);
        //组织数据结构发送会员注册消息
        MemberRegisterMsg memberRegisterMsg = new MemberRegisterMsg();
        memberRegisterMsg.setMember(member);
        //因为是页面重定向后再调用了登录接口，uuid发生变化，需要传递之前的uuid供分销使用
        memberRegisterMsg.setUuid(loginUserDTO.getOldUuid());
        this.messageSender.send(memberRegisterMsg);
        return member;
    }

    private void addConnect(LoginUserDTO loginUserDTO, Member member) {
        //写入UnionId
        ConnectDO connect = new ConnectDO();
        connect.setMemberId(member.getMemberId());
        connect.setUnionType(loginUserDTO.getUnionType().value());
        connect.setUnionId(loginUserDTO.getUnionid());
        connectMapper.insert(connect);
        if (!StringUtil.isEmpty(loginUserDTO.getOpenid())) {
            //写入openId
            connect = new ConnectDO();
            connect.setMemberId(member.getMemberId());
            connect.setUnionType(loginUserDTO.getOpenType().value());
            connect.setUnionId(loginUserDTO.getOpenid());
            connectMapper.insert(connect);
        }
    }

    /**
     * 根据用户id查询用户信息
     *
     * @param memberId
     * @return
     */
    private Member findMemberById(Long memberId) {
        Member member = memberManager.getModel(memberId);
        return member;
    }

    /**
     * 生成member的token
     *
     * @param member
     * @param uuid
     * @return
     */
    private MemberVO convertWechatMember(Member member, String uuid, Integer tokenOutTime, Integer refreshTokenOutTime) {
        //校验当前账号是否被禁用
        member.checkDisable();
        //新建买家用户角色对象
        Buyer buyer = new Buyer();
        //设置用户ID
        buyer.setUid(member.getMemberId());
        //设置用户名称
        buyer.setUsername(member.getUname());
        //设置uuid
        buyer.setUuid(uuid);
        //创建Token
        Token token = tokenManager.create(buyer, tokenOutTime, refreshTokenOutTime);
        //获取访问Token
        String accessToken = token.getAccessToken();
        //获取刷新Token
        String refreshToken = token.getRefreshToken();
        //组织返回数据
        MemberVO memberVO = new MemberVO(member, accessToken, refreshToken);
        return memberVO;
    }


    public MemberVO connectWeChatLoginHandle(Member member, String uuid, Integer tokenOutTime, Integer refreshTokenOutTime) {
        //初始化会员信息
        MemberVO memberVO = this.convertWechatMember(member, uuid, tokenOutTime, refreshTokenOutTime);
        //发送登录消息
        this.sendMessage(member, 1);
        return memberVO;
    }

    /**
     * 发送登录消息
     *
     * @param member
     * @param memberOrSeller
     */
    private void sendMessage(Member member, Integer memberOrSeller) {
        MemberLoginMsg loginMsg = new MemberLoginMsg();
        loginMsg.setLastLoginTime(member.getLastLogin());
        loginMsg.setMemberId(member.getMemberId());
        loginMsg.setMemberOrSeller(memberOrSeller);
        this.messageSender.send(loginMsg);
    }

}
