/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.statistics;

import cn.shoptnt.model.base.vo.BackendIndexModelVO;
import cn.shoptnt.model.statistics.vo.MultipleChart;

/**
 * 首页相关统计
 *
 * @author 张崧
 * @since 2024-04-24
 */
public interface IndexPageStatisticManager {

    /**
     * 首页统计
     * @return
     */
    BackendIndexModelVO index();

    /**
     * 访问流量图表
     *
     * @param startTime 开始时间
     * @param endTime   结束时间
     * @return
     */
    MultipleChart getPvChart(Long startTime, Long endTime);
}
