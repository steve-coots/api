/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.member;

import cn.shoptnt.model.member.dos.Member;
import cn.shoptnt.model.member.dto.MemberQueryParam;
import cn.shoptnt.model.member.dto.MemberStatisticsDTO;
import cn.shoptnt.model.member.vo.BackendMemberVO;
import cn.shoptnt.model.member.vo.MemberPointVO;
import cn.shoptnt.model.member.vo.MemberVO;
import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.security.ScanModuleDTO;
import cn.shoptnt.model.security.ScanResult;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * 会员业务层
 *
 * @author zh
 * @version v2.0
 * @since v7.0.0
 * 2018-03-16 11:33:56
 */
public interface MemberManager extends IService<Member> {

    /**
     * 查询会员列表
     *
     * @param memberQueryParam 查询条件
     * @return
     */
    WebPage list(MemberQueryParam memberQueryParam);

    /**
     * 修改会员登录次数
     * @param memberId 会员id
     * @param now 当前时间
     */
    void updateLoginNum(Long memberId, Long now);
    /**
     * 修改会员
     *
     * @param member 会员
     * @param id     会员主键
     * @return Member 会员
     */
    Member edit(Member member, Long id);

    /**
     * 禁用会员
     * @param id 会员ID
     */
    void disable(Long id);

    /**
     * 发送解禁会员消息
     * @param id 会员ID
     */
    void recoverySendMsg(Long id);

    /**
     * 删除会员
     *
     * @param clazz Member类
     * @param id    会员主键
     */
    void delete(Class<Member> clazz, Long id);

    /**
     * 获取会员
     *
     * @param id 会员表主键
     * @return Member  会员
     */
    Member getModel(Long id);

    /**
     * 根据用户名查询会员
     *
     * @param uname 用户名
     * @return 会员信息
     */
    Member getMemberByName(String uname);

    /**
     * 根据用户手机号码查询会员
     *
     * @param mobile 手机号码
     * @return 会员信息
     */
    Member getMemberByMobile(String mobile);

    /**
     * 根据邮箱获取用户
     *
     * @param email 电子邮箱
     * @return
     */
    Member getMemberByEmail(String email);

    /**
     * 查询当前会员的积分
     *
     * @return 会员vo
     */
    MemberPointVO getMemberPoint();

    /**
     * 生成用户名
     *
     * @param uname 用户填写的用户名
     * @return 用户名数组
     */
    String[] generateMemberUname(String uname);

    /**
     * 会员注册
     *
     * @param member 会员
     * @return 会员信息
     */
    Member register(Member member);

    /**
     * 会员用户名密码登录
     *
     * @param username 用户名
     * @param password 密码
     * @param memberOrSeller 会员还是卖家，1 会员  2 卖家
     * @return 是否登录成功
     */
    MemberVO login(String username, String password, Integer memberOrSeller);

    /**
     * 动态登录
     *
     * @param phone 手机号码
     * @param smsCode
     * @return 是否登录成功
     */
    MemberVO mobileLogin(String phone, String smsCode, Integer memberOrSeller);

    /**
     * 会员退出
     */
    void logout();

    /**
     * 登录会员后的处理
     *
     * @param member 会员信息
     * @param memberOrSeller 会员还是卖家，1 会员  2 卖家
     * @return
     */
    MemberVO loginHandle(Member member,Integer memberOrSeller);

    /**
     * 联合登录后处理
     *
     * @param member 会员信息
     * @param uuid   uuid
     * @return 会员信息
     */
    MemberVO connectLoginHandle(Member member, String uuid);


    /**
     * 验证会员账号密码的正确性(只验证不登录)
     *
     * @param username 用户名/手机号/邮箱
     * @param password 密码
     * @return 正确 true 错误 false
     */
    Member validation(String username, String password);

    /**
     * 根据账号获取当前会员的一些信息，供找回密码使用
     *
     * @param account 用户名/手机号/邮箱
     * @return 会员信息
     */
    Member getMemberByAccount(String account);

    /**
     * 获取当前会员的一些附属统计数
     * 比如：会员订单数、会员商品收藏数、会员店铺收藏数
     *
     * @return 会员统计信息
     */
    MemberStatisticsDTO getMemberStatistics();

    /**
     * 查询新的会员
     *
     * @param length 查询数量
     * @return
     */
    List<BackendMemberVO> newMember(Integer length);

    /**
     * 根据会员ids获取会员的集合
     *
     * @param memberIds 会员id数组
     * @return 会员信息
     */
    List<Member> getMemberByIds(Long[] memberIds);

    /**
     * 登录次数归零
     */
    void loginNumToZero();

    /**
     * 退出登录
     * @param memberId 会员id
     */
    void memberLoginout(Long memberId);

    /**
     * 查询所有会员id
     * @return
     */
    List<String> queryAllMemberIds();

    /**
     * 获取会员信息
     * @param memberId 会员ID
     * @return
     */
    Member getMember(Long memberId);

    /**
     * 休眠最后登陆时间为指定时间以前的账户
     * @param timeStamp 时间
     */
    void sleepMember(Long timeStamp);

    /**
     * 会员签名数据扫描
     * @return
     */
    ScanResult scanModule(ScanModuleDTO scanModuleDTO);

    /**
     * 会员数据重新签名
     */
    void reSign();

    /**
     * 修复会员数据
     * @param memberId
     */
    void repair(Long memberId);

    /**
     * 获取会员账户锁定状态
     * @param memberId 会员id
     * @return true 已锁定；false 未锁定
     */
    Boolean getLockStatus(Long memberId);

    /**
     * 解除会员账户锁定状态
     * @param memberId 会员id
     */
    void unLock(Long memberId);

    /**
     * 用户注销
     * @param smsCode 注销申请验证码
     */
    void logOff(String smsCode);

    /**
     * 查询会员数量
     * @return 会员数量
     */
    Integer getCount(Long startTime, Long endTime);
}
