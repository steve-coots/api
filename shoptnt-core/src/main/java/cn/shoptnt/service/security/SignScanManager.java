/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.security;

/**
 * 签名数据扫描接口
 * 会调用各个安全模块的任务去执行扫描
 * @author 妙贤
 * @data 2021/11/22 22:38
 * @version 1.0
 **/
public interface SignScanManager {

     /**
      * 扫描数据签名接口
      */
     void scan();

     /**
      * 对所有数据重新签名
      */
     void reSign();
}
