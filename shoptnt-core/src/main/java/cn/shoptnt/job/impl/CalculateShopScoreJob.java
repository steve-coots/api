/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.job.impl;

import cn.shoptnt.job.EveryDayExecute;
import cn.shoptnt.client.member.ShopClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 * 计算店铺评分
 *
 * @author fk
 * @version v1.0
 * @since v7.0.0
 * 2018-03-29 15:10:38
 */
@Component
public class CalculateShopScoreJob implements EveryDayExecute {

	@Autowired
	private ShopClient shopClient;

    @Override
	public void everyDay() {
		shopClient.calculateShopScore();
	}

}
