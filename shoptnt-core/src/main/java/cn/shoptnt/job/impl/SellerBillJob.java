/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.job.impl;

import cn.shoptnt.job.EveryMonthExecute;
import cn.shoptnt.client.trade.BillClient;
import cn.shoptnt.framework.util.DateUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 商家结算单生成 状态修改
 *
 * @author Chopper
 * @version v1.0
 * @since v7.0
 * 2018-07-19 下午2:49
 */
@Component
public class SellerBillJob implements EveryMonthExecute {

    private final Logger logger = LoggerFactory.getLogger(getClass());


    @Autowired
    private BillClient billClient;

    /**
     * 每月执行
     */
    @Override
    public void everyMonth() {
        try {
            this.logger.debug("-----生成结算单执行-----");
            // 上个月的开始结束时间
            Long[] time = DateUtil.getLastMonth();
            billClient.createBills(time[0],time[1]);
        } catch (Exception e) {
            this.logger.error("每月生成结算单异常：", e);
        }
    }
}
