/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.job.impl;

import cn.shoptnt.client.member.MemberClient;
import cn.shoptnt.client.system.SettingClient;
import cn.shoptnt.job.EveryDayExecute;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import cn.shoptnt.framework.util.CurrencyUtil;
import cn.shoptnt.framework.util.DateUtil;
import cn.shoptnt.framework.util.JsonUtil;
import cn.shoptnt.model.base.SettingGroup;
import cn.shoptnt.model.system.vo.AccountSetting;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 账户安全定时任务-长时间不访问账户自动休眠
 *
 * @author fk
 * @version v1.0
 * @since v7.0
 * 2018-07-05 下午2:11
 */
@Component
public class AccountSafeJob implements EveryDayExecute {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private SettingClient settingClient;

    @Autowired
    private MemberClient memberClient;

    /**
     * 每晚23:30执行
     */
    @Override
    public void everyDay() {
        try{
            logger.debug("长时间不访问账户自动休眠任务开始执行");
            String accountSettingJson = settingClient.get( SettingGroup.ACCOUNT);
            AccountSetting accountSetting = JsonUtil.jsonToObject(accountSettingJson, AccountSetting.class);
            Integer sleepTime = accountSetting.getSleepThreshold();
            long timeStamp = (long) CurrencyUtil.sub(DateUtil.getDateline(),CurrencyUtil.mul(sleepTime,86400));
            memberClient.sleepMember(timeStamp);
        }catch (Exception e) {
            logger.error("长时间不访问账户自动休眠任务异常", e);
        }

    }

}
