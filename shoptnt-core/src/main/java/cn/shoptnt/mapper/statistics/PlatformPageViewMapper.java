/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.mapper.statistics;

import cn.shoptnt.model.statistics.dos.PlatformPageView;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.conditions.query.LambdaQueryChainWrapper;
import com.baomidou.mybatisplus.extension.conditions.query.QueryChainWrapper;

/**
 * 平台访问量统计Mapper
 *
 * @author 张崧
 * @since 2024-04-24
 */
public interface PlatformPageViewMapper extends BaseMapper<PlatformPageView> {

    /**
     * 查询时间段内的访问量
     *
     * @param startTime 开始时间
     * @param endTime   结束时间
     * @return 访问量
     */
    default Integer selectVisitCountByTimeRange(Long startTime, Long endTime) {
        PlatformPageView one = new QueryChainWrapper<>(this)
                .select("sum(num) as num")
                .ge("time", startTime)
                .le("time", endTime)
                .one();
        return one == null || one.getNum() == null ? 0 : one.getNum();
    }

    /**
     * 按日期查询
     * @param time 日期时间戳
     * @return
     */
    default PlatformPageView selectByTime(Long time){
        return new LambdaQueryChainWrapper<>(this).eq(PlatformPageView::getTime, time).one();
    }
}
