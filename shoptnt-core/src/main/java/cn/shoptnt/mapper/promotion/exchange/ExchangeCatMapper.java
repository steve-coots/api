/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.mapper.promotion.exchange;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.shoptnt.model.promotion.exchange.dos.ExchangeCat;

/**
 * 积分兑换商品分类Mapper接口
 * @author duanmingyu
 * @version v1.0
 * @since v7.2.2
 * 2020-08-10
 */
//@CacheNamespace(implementation= MybatisRedisCache.class,eviction=MybatisRedisCache.class)
public interface ExchangeCatMapper extends BaseMapper<ExchangeCat> {
}
