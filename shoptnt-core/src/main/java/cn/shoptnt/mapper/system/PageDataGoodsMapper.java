/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.mapper.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.shoptnt.model.pagedata.PageData;
import cn.shoptnt.model.pagedata.PageDataGoods;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
 * 楼层商品中间表mapper
 * @author fk
 * @version v1.0
 * @since v7.2.2
 * 2021年12月10日16:56:50
 */
//@CacheNamespace(implementation= MybatisRedisCache.class,eviction=MybatisRedisCache.class)
public interface PageDataGoodsMapper extends BaseMapper<PageDataGoods>  {

    /**
     * 查询关联了某些商品的微页面数据
     * @param goodsIds
     * @return
     */
    List<PageData> selectPageList(@Param("goodsIds") Long[] goodsIds);
}
