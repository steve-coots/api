/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.mapper.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.shoptnt.model.system.dos.AuthenticationDO;


/**
 * 二次身份验证Mapper
 *
 * @author shenyanwu
 * @version v2.0
 * @since v2.0
 * 2021-11-19 18:45:02
 */
public interface AuthenticationMapper extends BaseMapper<AuthenticationDO> {


}