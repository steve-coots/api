/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.mapper.goods;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import cn.shoptnt.model.goods.dos.CategoryDO;
import org.apache.ibatis.annotations.Param;

/**
 * 分类Category的Mapper
 * @author fk
 * @version 1.0
 * @since 7.1.0
 * 2020/7/21
 */
//@CacheNamespace(implementation= MybatisRedisCache.class,eviction=MybatisRedisCache.class)
public interface CategoryMapper extends BaseMapper<CategoryDO> {

    /**
     * 根据商品ID获取分类信息
     * @param goodsId 商品ID
     * @return
     */
    CategoryDO selectCategoryByGoodsId(@Param("goodsId") Long goodsId);


}
