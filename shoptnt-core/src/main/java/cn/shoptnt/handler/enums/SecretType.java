/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.handler.enums;

/**
 * 加密的秘钥类型
 * @author fk
 * @version 5.2.3
 * @data 2021年12月17日14:28:38
 **/
public enum SecretType {

    /**
     * 没有
     */
    NO,

    /**
     * 身份证
     */
    ID_CARD,

    /**
     * 银行卡
     */
    BANK,

    /**
     * 手机号
     */
    MOBILE;

}
