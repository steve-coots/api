/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.statistics;

import cn.shoptnt.framework.test.TestConfig;
import cn.shoptnt.model.base.SearchCriteria;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * 会员相关统计业务层测试
 * @author zs
 * @version 1.0
 * @since 7.2.2
 * 2020/08/04
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {TestConfig.class})
@MapperScan(basePackages = "cn.shoptnt.mapper")
public class MemberStatisticManagerTest {

    @Autowired
    private MemberStatisticManager memberStatisticManager;

    @Test
    public void getIncreaseMember() {

        
        
        
    }

    @Test
    public void getIncreaseMemberPage() {

        
        
        
    }

    @Test
    public void getMemberOrderQuantity() {

        
        
        
    }

    @Test
    public void getMemberOrderQuantityPage() {

        
        
        
    }

    @Test
    public void getMemberGoodsNum() {

        
        
        
    }

    @Test
    public void getMemberGoodsNumPage() {

        
        
        
    }

    @Test
    public void getMemberMoney() {

        
        
        
    }

    @Test
    public void getMemberMoneyPage() {

        
        
        
    }

    private SearchCriteria getSearchCriteria1(){
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setCycleType("YEAR");
        searchCriteria.setYear(2019);
        searchCriteria.setMonth(12);
        searchCriteria.setCategoryId(0l);
        searchCriteria.setSellerId(17l);

        return searchCriteria;
    }

    private SearchCriteria getSearchCriteria2(){
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setCycleType("YEAR");
        searchCriteria.setYear(2020);
        searchCriteria.setMonth(12);
        searchCriteria.setCategoryId(555l);
        searchCriteria.setSellerId(17l);

        return searchCriteria;
    }

    private SearchCriteria getSearchCriteria3(){
        SearchCriteria searchCriteria = new SearchCriteria();
        searchCriteria.setCycleType("YEAR");
        searchCriteria.setYear(2020);
        searchCriteria.setMonth(12);
        searchCriteria.setCategoryId(0l);
        searchCriteria.setSellerId(17l);

        return searchCriteria;
    }

}
