/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.service.payment;

import cn.shoptnt.framework.test.TestConfig;
import cn.shoptnt.mapper.payment.PaymentMethodMapper;
import cn.shoptnt.model.payment.dos.PaymentMethodDO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 支付方式业务层测试
 * @author zs
 * @version 1.0
 * @since 7.2.2
 * 2020/07/30
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@ContextConfiguration(classes = {TestConfig.class})
@MapperScan(basePackages = "cn.shoptnt.mapper")
public class PaymentMethodManagerTest {

    @Autowired
    private PaymentMethodManager paymentMethodManager;
    @Autowired
    private PaymentMethodMapper paymentMethodMapper;

    @Test
    public void add() {
        Map delMap = new HashMap();
        delMap.put("plugin_id", "xxx");
        paymentMethodMapper.deleteByMap(delMap);
    }

    @Test
    public void delete() {
        paymentMethodMapper.deleteById(1l);
    }

    @Test
    public void getByPluginId() {

        PaymentMethodDO weixinPayPlugin = paymentMethodManager.getByPluginId("weixinPayPlugin");

        
    }

    @Test
    public void queryMethodByClient() {

        List<Map<String,Object>> list = paymentMethodMapper.selectMaps(null);

        
    }

    @Test
    public void getConfig() {

        String s = paymentMethodMapper.selectClientType("pc_config", "weixinPayPlugin");

        
    }

}
