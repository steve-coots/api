package cn.shoptnt.framework.trigger.standlone;

import cn.shoptnt.framework.context.ApplicationContextHolder;
import cn.shoptnt.framework.trigger.Interface.TimeTriggerExecuter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.Serializable;
import java.util.TimerTask;

/**
 * 本地延迟任务实现
 * @author kingapex
 * @version 1.0
 * @data 2022/10/27 11:48
 **/
public class LocalTimerTask extends TimerTask implements Serializable {
    private static final long serialVersionUID = -5676352194131639L;

    private static Logger logger = LoggerFactory.getLogger(LocalTimerTask.class);

    private Object param;
    private String uniqueKey;
    private String executerName;

    public LocalTimerTask(String uniqueKey, String executerName,Object param) {
        this.param = param;
        this.uniqueKey = uniqueKey;
        this.executerName = executerName;
    }


    @Override
    public void run() {
        logger.debug("触发延迟任务{}",uniqueKey);
        try {

            //触发后删除持久化数据
            StandaloneTrigger standaloneTrigger = (StandaloneTrigger) ApplicationContextHolder.getBean("standaloneTrigger");
            standaloneTrigger.delete(uniqueKey);

            //触发延迟任务
            TimeTriggerExecuter timeTriggerExecuter = (TimeTriggerExecuter) ApplicationContextHolder.getBean(executerName);
            timeTriggerExecuter.execute(param);


        }catch (Exception e) {
            logger.error("触发延迟任务{},失败", uniqueKey,e);
        }
    }

    @Override
    public String toString() {
        return "LocalTimerTask{" +
                "param=" + param +
                ", uniqueKey='" + uniqueKey + '\'' +
                ", executerName='" + executerName + '\'' +
                '}';
    }
}
