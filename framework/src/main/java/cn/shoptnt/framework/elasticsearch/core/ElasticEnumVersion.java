/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.framework.elasticsearch.core;

/**
 *  ES 版本枚举类
 * @author liuyulei
 * @version 1.0
 * @since 7.2.2
 * 2021/1/21  17:40
 */
public enum ElasticEnumVersion {

    /**
     * 目前兼容版本  比如7.9  表示兼容7.9.X版本  其中.X小版本没有进行具体的测试  只进行了.0的测试
     * 其中7.9.3为线上文档默认提供版本
     */
    ES7_10("7.10"),
    ES7_9("7.9"),
    ES7_8("7.8"),
    ES7_7("7.7"), 
    ES7_6("7.6"), 
    ES7_5("7.5"), 
    ES7_4("7.4"), 
    ES7_3("7.3"), 
    ES7_2("7.2"), 
    ES7_1("7.1"), 
    ES7_0("7.0"), 
    ES6_8("6.8"), 
    ES6_7("6.7"),
    ES6_6("6.6"),  
    ES6_5("6.5"),  
    ES6_4("6.4"),

    /**
     * ES 7与6的创建mapping方式有区别，以下判断大版本号进行区分创建映射
     */
    ES7("7"),
    ES6("6");


    private String version;



    ElasticEnumVersion(String version) {
        this.version = version;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }



}
