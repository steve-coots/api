/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.framework.sncreator;

/**
 * @author 妙贤
 * @version 1.0
 * @since 7.1.0
 * 2019-11-22
 */
public interface SnCreator {

    /**
     * 生成唯一数字
     * @return
     */
    Long create(int subId);

}
