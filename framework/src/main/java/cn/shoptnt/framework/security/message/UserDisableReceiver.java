/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.framework.security.message;

import cn.shoptnt.framework.message.broadcast.BroadcastMessage;
import cn.shoptnt.framework.message.broadcast.BroadcastChannel;
import cn.shoptnt.framework.message.broadcast.BroadcastMessageReceiver;
import cn.shoptnt.framework.security.AuthenticationService;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 用户禁用消息接收器
 * @author 妙贤
 * @version 1.0
 * @since 7.1.0
 * 2019/12/27
 * spring:
 *   application:
 *     name: base-api
 */

@Component
public class UserDisableReceiver implements BroadcastMessageReceiver {

    public UserDisableReceiver(List<AuthenticationService> authenticationServices) {
        this.authenticationServices = authenticationServices;
    }

    private List<AuthenticationService> authenticationServices;

    @Override
    public String getChannelName() {
        return BroadcastChannel.USER_DISABLE;
    }

    @Override
    public void receiveMsg(BroadcastMessage message) {
        UserDisableMsg userDisableMsg = (UserDisableMsg) message;
        if (authenticationServices != null) {
            for (AuthenticationService authenticationService : authenticationServices) {
                authenticationService.userDisableEvent(userDisableMsg);
            }
        }

    }
}
