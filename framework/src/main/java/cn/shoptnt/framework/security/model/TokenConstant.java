/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.framework.security.model;
/**
 * 定义Token基本常量
 * @author zh
 * @version v7.0
 * @date 18/4/12 下午2:46
 * @since v7.0
 */
public class TokenConstant {
    public static final String TOKEN_PREFIX = "Bearer";
    public static final String HEADER_STRING = "Authorization";
    public static final String HOST = "Host";

    /**时间戳失效时间，单位：秒*/
    public static final int INVALID_TIME = 60;


    /**
     * 服务器端加密秘钥，主要用于将手机号等加密存储到数据库中
     */
    public static final String SECRET = "ed3s5w7e8rhgfwqs";
}
