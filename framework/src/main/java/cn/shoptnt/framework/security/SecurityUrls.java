package cn.shoptnt.framework.security;

import java.util.ArrayList;
import java.util.List;

/**
 * 公用权限url定义
 *
 * @author kingapex
 * @version 1.0
 * @data 2022/10/31 17:45
 **/
public abstract class SecurityUrls {

    private SecurityUrls() {
    }

    private static List<String> commonUrls = new ArrayList<String>();

    static {
        commonUrls.add("/v3/api-docs**");
        commonUrls.add("/configuration/ui");
        commonUrls.add("/swagger-resources/**");
        commonUrls.add("/configuration/security");
        commonUrls.add("/swagger-ui/**");
        commonUrls.add("/webjars/**");
        commonUrls.add("/actuator/**");
    }

    public static List<String> getCommonUrls() {
        return commonUrls;
    }
}

