package cn.shoptnt.framework.lock.standalone;

import cn.shoptnt.framework.lock.Lock;
import cn.shoptnt.framework.lock.LockFactory;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 本地锁工厂实现
 * @author kingapex
 * @version 1.0
 * @data 2022/10/24 15:13
 **/
@Service
@ConditionalOnProperty(value = "shoptnt.runmode", havingValue = "standalone")
public class LocalLockFactory implements LockFactory {

    private static Map<String, LocalLock> lockMap = new HashMap<>();

    @Override
    public Lock getLock(String lockName) {

        //线程安全，用锁名上锁
        //如果map中没有，new一个，否则返回
        synchronized (lockName) {
            LocalLock lock = lockMap.get(lockName);
            if (lock == null) {
                lock = new LocalLock(new ReentrantLock());
                lockMap.put(lockName, lock);
            }
            return lock;
        }

    }
}
