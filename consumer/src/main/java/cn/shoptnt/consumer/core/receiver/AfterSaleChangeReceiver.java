/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.consumer.core.receiver;

import cn.shoptnt.framework.message.direct.AmqpExchange;
import cn.shoptnt.message.dispatcher.trade.aftersale.AfterSaleChangeDispatcher;
import cn.shoptnt.model.base.message.AfterSaleChangeMessage;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * 售后服务单状态变化接收者
 *
 * @author duanmingyu
 * @version v1.0
 * @since v7.1.5
 * 2019-10-24
 */
@Component
public class AfterSaleChangeReceiver {

    @Autowired
    private AfterSaleChangeDispatcher dispatcher;

    /**
     * 处理售后服务单状态变化消息
     *
     * @param afterSaleChangeMessage
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.AS_STATUS_CHANGE + "_QUEUE"),
            exchange = @Exchange(value = AmqpExchange.AS_STATUS_CHANGE, type = ExchangeTypes.FANOUT)
    ))
    public void afterSaleChange(AfterSaleChangeMessage afterSaleChangeMessage) {
        dispatcher.dispatch(afterSaleChangeMessage);
    }

}
