/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.consumer.core.receiver;

import cn.shoptnt.framework.message.direct.AmqpExchange;
import cn.shoptnt.message.dispatcher.goods.GoodsWordsChangeDispatcher;
import cn.shoptnt.model.base.message.GoodsWordChangeMessage;
import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @author liuyulei
 * @version 1.0
 * @Description: 提示词变化消息接收者
 * @date 2019/6/13 17:15
 * @since v7.0
 */
@Component
public class GoodsWordsChangeReceiver {

    @Autowired
    private GoodsWordsChangeDispatcher dispatcher;

    /**
     * 提示词变化
     *
     * @param message
     */
    @RabbitListener(bindings = @QueueBinding(
            value = @Queue(value = AmqpExchange.GOODS_WORDS_CHANGE + "_QUEUE"),
            exchange = @Exchange(value = AmqpExchange.GOODS_WORDS_CHANGE, type = ExchangeTypes.FANOUT)
    ))
    public void goodsChange(GoodsWordChangeMessage message) {
        dispatcher.dispatch(message);
    }
}
