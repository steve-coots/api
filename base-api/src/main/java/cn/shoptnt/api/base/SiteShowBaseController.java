/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.base;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import cn.shoptnt.model.base.SettingGroup;
import cn.shoptnt.service.base.service.SettingManager;
import cn.shoptnt.model.system.vo.SiteSetting;
import cn.shoptnt.framework.util.JsonUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 站点信息首页展示控制器
 *
 * @author zh
 * @version v7.0
 * @date 18/7/13 上午11:21
 * @since v7.0
 */
@RestController
@RequestMapping("/base/site-show")
@Tag(name = "站点展示")
public class SiteShowBaseController {

    @Autowired
    private SettingManager settingManager;
    @GetMapping
    @Operation(summary = "获取站点设置")
    public SiteSetting getSiteSetting(){
        String siteJson = settingManager.get(SettingGroup.SITE);
        return JsonUtil.jsonToObject(siteJson,SiteSetting.class);
    }
}
