/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.base;

import cn.shoptnt.framework.util.Base64;
import cn.shoptnt.framework.util.NetImageUtil;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
/**
 * @author liuyulei
 * @version 1.0
 * @since 7.2.2
 * 2020/11/17 0017  10:32
 */
@RestController
@RequestMapping("/base/net-image")
@Tag(name = "网络图片处理API")
public class NetImagesBaseController {

    @GetMapping(value = "/base64")
    @Operation(summary = "获取网络图片的base64编码")
    @Parameters({
            @Parameter(name = "url", description = "网络图片地址", required = true,   in = ParameterIn.QUERY)
    })
    public String base64Images(String url) {
        byte[] urlByte = NetImageUtil.getImageFromNetByUrl(url);
        String base64Image = Base64.encode(urlByte);
        return base64Image;
    }
}
