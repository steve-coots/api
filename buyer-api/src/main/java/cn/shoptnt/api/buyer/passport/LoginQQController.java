/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.passport;


import cn.shoptnt.model.member.dto.QQUserDTO;
import cn.shoptnt.service.passport.LoginQQManager;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * 微信统一登陆
 *
 * @author cs
 * @version v1.0
 * @since v7.2.2
 * 2019-09-22
 */
@Tag(name = "QQ统一登陆")
@RestController
@RequestMapping("/buyer/connect/qq")
@Validated
public class LoginQQController {

    @Autowired
    private LoginQQManager loginQQManager;


    @Operation(summary = "获取appid")
    @GetMapping("/wap/getAppid")
    public String getLoginUrl(){
        return  loginQQManager.getAppid();
    }

    @Operation(summary = "网页登陆")
    @GetMapping("/wap/login")
    public Map h5Login(String access_token,String uuid){
        return loginQQManager.qqWapLogin(access_token,uuid);
    }

    @Operation(summary = "app登陆")
    @PostMapping("/app/login/{uuid}")
    public Map appLogin(@PathVariable String uuid, QQUserDTO qqUserDTO){
        return loginQQManager.qqAppLogin(uuid,qqUserDTO);
    }

}
