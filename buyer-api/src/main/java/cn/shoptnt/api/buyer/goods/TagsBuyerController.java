/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.goods;

import cn.shoptnt.model.support.validator.annotation.MarkType;
import cn.shoptnt.model.goods.dos.TagsDO;
import cn.shoptnt.model.goods.vo.BuyCountVO;
import cn.shoptnt.model.goods.vo.GoodsSelectLine;
import cn.shoptnt.model.goods.vo.TagGoodsNum;
import cn.shoptnt.service.goods.GoodsQueryManager;
import cn.shoptnt.service.goods.TagsManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 商品标签控制器
 *
 * @author fk
 * @version v2.0
 * @since v7.0.0
 * 2018-03-28 14:49:36
 */
@RestController
@RequestMapping("/buyer/goods")
@Tag(name = "标签商品相关API")
@Validated
public class TagsBuyerController {

    @Autowired
    private TagsManager tagsManager;

    @Autowired
    private GoodsQueryManager goodsQueryManager;

    @Operation(summary = "查询标签商品列表")
    @Parameters({
            @Parameter(name = "seller_id", description = "卖家id", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "mark", description = "hot热卖 new新品 recommend推荐", required = true,   in = ParameterIn.PATH),
            @Parameter(name = "num", description = "查询数量", required = true,  in = ParameterIn.QUERY)
    })
    @GetMapping("/tags/{mark}/goods")
    public List<GoodsSelectLine> list(@Parameter(hidden = true) @NotNull(message = "店铺不能为空") Long sellerId, Integer num, @MarkType @PathVariable String mark) {

        if (num == null) {
            num = 5;
        }

        return tagsManager.queryTagGoods(sellerId, num, mark);
    }



    @Operation(summary = "查询商品销量")
    @Parameters({
            @Parameter(name = "goods_id", description = "商品id", required = true,  in = ParameterIn.QUERY)
    })
    @GetMapping("/tags/count")
    public List<BuyCountVO> list(@Parameter(hidden = true) @RequestParam("goods_id") Long[] goodsId) {
        return goodsQueryManager.queryBuyCount(goodsId);
    }

    @Operation(summary = "查询某店铺标签商品的数量")
    @Parameter(name = "shop_id", description = "店铺id", required = true, in = ParameterIn.PATH)
    @GetMapping(value = "/tags/{shop_id}/goods-num")
    public TagGoodsNum tagGoodsNum(@PathVariable("shop_id") Long shopId){

        return tagsManager.queryGoodsNumByShopId(shopId);
    }

}
