/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.member;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.base.CharacterConstant;
import cn.shoptnt.model.member.dos.AskReplyDO;
import cn.shoptnt.model.member.dos.MemberAsk;
import cn.shoptnt.model.member.dto.ReplyQueryParam;
import cn.shoptnt.model.member.enums.AuditEnum;
import cn.shoptnt.model.member.enums.CommonStatusEnum;
import cn.shoptnt.model.member.vo.AskReplyVO;
import cn.shoptnt.service.member.AskReplyManager;
import cn.shoptnt.model.util.sensitiveutil.SensitiveFilter;
import cn.shoptnt.framework.context.user.UserContext;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * 会员商品咨询回复API
 *
 * @author duanmingyu
 * @version v1.0
 * @since v7.1.5
 * 2019-09-17
 */
@RestController
@RequestMapping("/buyer/members/asks/reply")
@Tag(name = "会员商品咨询回复API")
@Validated
public class AskReplyBuyerController {

    @Autowired
    private AskReplyManager askReplyManager;

    @Operation(summary = "查询某条会员商品咨询回复列表")
    @Parameters({
            @Parameter(name = "page_no", description = "页码", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "ask_id", description = "会员商品咨询id", required = true,  in = ParameterIn.PATH),
            @Parameter(name = "reply_id", description = "会员商品咨询回复id",  in = ParameterIn.PATH)
    })
    @GetMapping("/list/{ask_id}")
    public WebPage listReply(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize, @PathVariable("ask_id")Long askId, @Parameter(hidden = true) Long replyId) {

        //会员商品咨询回复搜索参数实体
        ReplyQueryParam param = new ReplyQueryParam();
        param.setPageNo(pageNo);
        param.setPageSize(pageSize);
        param.setAskId(askId);
        param.setReplyId(replyId);
        param.setAuthStatus(AuditEnum.PASS_AUDIT.value());
        param.setReplyStatus(CommonStatusEnum.YES.value());

        return this.askReplyManager.list(param);
    }

    @Operation(summary = "查询会员商品咨询回复列表")
    @Parameters({
            @Parameter(name = "page_no", description = "页码", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "reply_status", description = "是否已回复 YES：是，NO：否",   in = ParameterIn.QUERY)
    })
    @GetMapping("/list/member")
    public WebPage listMemberReply(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize, @Parameter(hidden = true) String replyStatus) {

        //会员商品咨询回复搜索参数实体
        ReplyQueryParam param = new ReplyQueryParam();
        param.setPageNo(pageNo);
        param.setPageSize(pageSize);
        param.setReplyStatus(replyStatus);
        param.setMemberId(UserContext.getBuyer().getUid());

        return this.askReplyManager.listMemberReply(param);
    }

    @Operation(summary = "回复会员商品咨询")
    @Parameters({
            @Parameter(name = "ask_id", description = "会员商品咨询id", required = true,  in = ParameterIn.PATH),
            @Parameter(name = "reply_content", description = "回复内容", required = true,   in = ParameterIn.QUERY),
            @Parameter(name = "anonymous", description = "是否匿名 YES:是，NO:否", required = true,   in = ParameterIn.QUERY)
    })
    @PostMapping("/{ask_id}")
    public AskReplyDO add(@PathVariable("ask_id")Long askId, @NotEmpty(message = "请输入内容")@Parameter(hidden = true) String replyContent, @NotNull(message = "请选择是否匿名") @Parameter(hidden = true) String anonymous) {

        //咨询回复敏感词过滤
        replyContent = SensitiveFilter.filter(replyContent, CharacterConstant.WILDCARD_STAR);

        return this.askReplyManager.updateReply(askId, replyContent, anonymous);
    }

    @Operation(summary = "删除回复")
    @Parameters({
            @Parameter(name = "id", description = "会员商品咨询回复id",  in = ParameterIn.PATH),
    })
    @DeleteMapping("/{id}")
    public String delete(@PathVariable("id") Long id) {

        this.askReplyManager.delete(id);

        return "";
    }
}
