/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.trade;

import cn.shoptnt.model.member.dos.MemberCoupon;
import cn.shoptnt.model.trade.cart.enums.CartType;
import cn.shoptnt.model.trade.cart.enums.CheckedWay;
import cn.shoptnt.model.trade.cart.vo.CartPromotionVo;
import cn.shoptnt.model.trade.cart.vo.CartView;
import cn.shoptnt.service.trade.cart.CartPromotionManager;
import cn.shoptnt.service.trade.cart.cartbuilder.CartBuilder;
import cn.shoptnt.service.trade.cart.cartbuilder.CartPriceCalculator;
import cn.shoptnt.service.trade.cart.cartbuilder.CartSkuRenderer;
import cn.shoptnt.service.trade.cart.cartbuilder.CheckDataRebderer;
import cn.shoptnt.service.trade.cart.cartbuilder.impl.DefaultCartBuilder;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import javax.validation.constraints.NotNull;

/**
 * 购物车价格计算接口
 *
 * @author Chopper
 * @version v1.0
 * @since v7.0
 * 2018-12-01 下午8:26
 */
@Tag(name = "购物车价格计算API")
@RestController
@RequestMapping("/buyer/trade/promotion")
@Validated
public class TradePromotionController {


    @Autowired
    private CartPromotionManager promotionManager;


    /**
     * 购物车价格计算器
     */
    @Autowired
    private CartPriceCalculator cartPriceCalculator;
    /**
     * 数据校验
     */
    @Autowired
    private CheckDataRebderer checkDataRebderer;

    /**
     * 购物车sku数据渲染器
     */
    @Autowired
    private CartSkuRenderer cartSkuRenderer;

    @Operation(summary = "选择要参与的促销活动")
    @Parameters({
            @Parameter(name = "seller_id", description = "卖家id", required = true, in = ParameterIn.QUERY),
            @Parameter(name = "sku_id", description = "产品id", required = true, in = ParameterIn.QUERY),
            @Parameter(name = "promotion_id", description = "活动id", required = true, in = ParameterIn.QUERY),
            @Parameter(name = "promotion_type", description = "活动类型", required = true, in = ParameterIn.QUERY),})
    @RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.POST)
    public void setPromotion(@Parameter(hidden = true) Long sellerId, @Parameter(hidden = true) Long skuId, @Parameter(hidden = true) Long promotionId, @Parameter(hidden = true) String promotionType) {
        CartPromotionVo promotionVo = new CartPromotionVo();
        promotionVo.setPromotionId(promotionId);
        promotionVo.setPromotionType(promotionType);
        promotionManager.usePromotion(sellerId, skuId, promotionVo);
    }


    @Operation(summary = "取消参与促销")
    @Parameters({
            @Parameter(name = "seller_id", description = "卖家id", required = true, in = ParameterIn.QUERY),
            @Parameter(name = "sku_id", description = "产品id", required = true, in = ParameterIn.QUERY)
    })
    @RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.DELETE)
    public void promotionCancel(Long sellerId, Long skuId) {
        promotionManager.delete(new Long[]{skuId});
    }

    /**
     * 使用优惠券的时候分为三种情况：前2种情况couponId 不为0,不为空。第3种情况couponId为0," +
     * "1、使用优惠券:在刚进入订单结算页，为使用任何优惠券之前。" +
     * "2、切换优惠券:在1、情况之后，当用户切换优惠券的时候。" +
     * "3、取消已使用的优惠券:用户不想使用优惠券的时候。
     *
     * @param sellerId
     * @param mcId
     * @param way
     */
    @Operation(summary = "设置优惠券")
    @Parameters({
            @Parameter(name = "seller_id", description = "店铺ID", required = true, in = ParameterIn.PATH),
            @Parameter(name = "mc_id", description = "优惠券ID", required = true, in = ParameterIn.PATH),
            @Parameter(name = "way", description = "结算方式，BUY_NOW：立即购买，CART：购物车", required = true)
    })
    @PostMapping(value = "/{seller_id}/seller/{mc_id}/coupon")
    public void setCoupon(@NotNull(message = "店铺id不能为空") @PathVariable("seller_id") Long sellerId,
                          @NotNull(message = "优惠券id不能为空") @PathVariable("mc_id") Long mcId,
                          @NotNull(message = "结算方式不能为空") @RequestParam String way) {

        CartBuilder cartBuilder = new DefaultCartBuilder(CartType.CART, cartSkuRenderer, null, cartPriceCalculator, checkDataRebderer);
        CartView cartViewold = cartBuilder.renderSku(CheckedWay.valueOf(way)).countPrice(false).build();
        //先判断优惠券能否使用
        MemberCoupon memberCoupon = promotionManager.detectCoupon(sellerId, mcId, cartViewold.getCartList());
        //查询要结算的某卖家的购物信息
        CartView cartView = cartBuilder.renderSku(CheckedWay.valueOf(way)).countPrice(true).build();
        //设置优惠券  goodsPrice
        promotionManager.useCoupon(sellerId, mcId, cartView.getCartList(), memberCoupon);
    }


}
