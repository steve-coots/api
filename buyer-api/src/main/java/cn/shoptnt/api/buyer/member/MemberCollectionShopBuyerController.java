/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.member;

import cn.shoptnt.model.base.vo.SuccessMessage;
import cn.shoptnt.model.member.dos.MemberCollectionShop;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import cn.shoptnt.framework.database.WebPage;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;


import javax.validation.constraints.NotNull;

import cn.shoptnt.service.member.MemberCollectionShopManager;

/**
 * 会员收藏店铺表控制器
 *
 * @author zh
 * @version v7.0.0
 * @since v7.0.0
 * 2018-03-30 20:34:23
 */
@RestController
@RequestMapping("/buyer/members")
@Tag(name = "会员收藏店铺表相关API")
@Validated
public class MemberCollectionShopBuyerController {

    @Autowired
    private MemberCollectionShopManager memberCollectionShopManager;


    @Operation(summary = "查询会员收藏店铺列表")
    @Parameters({
            @Parameter(name = "page_no", description = "页码", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量", required = true,  in = ParameterIn.QUERY)
    })
    @GetMapping("/collection/shops")
    public WebPage list(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize) {
        return this.memberCollectionShopManager.list(pageNo, pageSize);
    }


    @Operation(summary = "添加会员收藏店铺")
    @Parameter(name = "shop_id", description = "店铺id", required = true,  in = ParameterIn.QUERY)
    @PostMapping("/collection/shop")
    public MemberCollectionShop add(@NotNull(message = "店铺id不能为空") @Parameter(hidden = true) Long shopId) {
        MemberCollectionShop memberCollectionShop = new MemberCollectionShop();
        memberCollectionShop.setShopId(shopId);
        try {
            return this.memberCollectionShopManager.add(memberCollectionShop);
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }


    @DeleteMapping(value = "/collection/shop/{shop_id}")
    @Operation(summary = "删除会员收藏店铺")
    @Parameters({
            @Parameter(name = "shop_id", description = "要删除的店铺id", required = true,  in = ParameterIn.PATH)
    })
    public String delete(@PathVariable("shop_id") Long shopId) {
        this.memberCollectionShopManager.delete(shopId);
        return "";
    }

    @GetMapping(value = "/collection/shop/{id}")
    @Operation(summary = "查看会员是否收藏店铺")
    @Parameters({
            @Parameter(name = "id", description = "要检索的收藏店铺id", required = true,  in = ParameterIn.PATH)
    })
    public SuccessMessage get(@PathVariable Long id) {
        SuccessMessage successMessage = new SuccessMessage(this.memberCollectionShopManager.isCollection(id));
        return successMessage;
    }

}
