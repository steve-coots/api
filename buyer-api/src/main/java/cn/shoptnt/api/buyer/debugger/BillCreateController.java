/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.debugger;

import cn.shoptnt.framework.util.DateUtil;
import cn.shoptnt.service.orderbill.BillManager;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.web.bind.annotation.*;


/**
 * @author lzg
 * @version v1.0
 * @Description: 结算单账单控制器
 * @date 2021/02/24 16:49
 * @since v7.0.0
 */
@RestController
@RequestMapping("/debugger")
@ConditionalOnProperty(value = "shoptnt.debugger", havingValue = "true")
public class BillCreateController {

    @Autowired
    private BillManager billManager;


    @GetMapping("/bills")
    @Parameters({
            @Parameter(name = "start_time", description = "开始时间 2021-01-01 00:00:01", required = true,   in = ParameterIn.QUERY),
            @Parameter(name = "end_time", description = "结束时间 2021-01-30 23:59:59", required = true,   in = ParameterIn.QUERY)
    })
    public String createBill(@Parameter(hidden = true) String startTime, @Parameter(hidden = true) String endTime) {

        this.billManager.createBills(DateUtil.getDateline(startTime, "yyyy-MM-dd HH:mm:ss"), DateUtil.getDateline(endTime, "yyyy-MM-dd HH:mm:ss"));
        return "结算单生成成功";
    }

}
