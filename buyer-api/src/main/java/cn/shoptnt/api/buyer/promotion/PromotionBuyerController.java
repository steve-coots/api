/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.promotion;

import cn.shoptnt.model.promotion.tool.vo.PromotionVO;
import cn.shoptnt.service.promotion.tool.PromotionGoodsManager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import java.util.List;

/**
 * 促销活动控制器
 *
 * @author Snow create in 2018/7/10
 * @version v2.0
 * @since v7.0.0
 */

@RestController
@RequestMapping("/buyer/promotions")
@Tag(name = "促销活动相关API")
@Validated
public class PromotionBuyerController {

    @Autowired
    private PromotionGoodsManager promotionGoodsManager;

    @Operation(summary = "根据商品读取参与的所有活动")
    @Parameters({
            @Parameter(name = "goods_id", description ="商品ID", required = true,  in = ParameterIn.PATH)
    })
    @GetMapping("/{goods_id}")
    public List<PromotionVO> getGoods(@Parameter(hidden = true) @PathVariable("goods_id") Long goodsId){
        List<PromotionVO> promotionVOList = this.promotionGoodsManager.getPromotion(goodsId);
        return promotionVOList;
    }
}
