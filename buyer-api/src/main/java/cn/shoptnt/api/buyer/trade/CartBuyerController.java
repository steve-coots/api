/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.buyer.trade;

import cn.shoptnt.model.errorcode.TradeErrorCode;
import cn.shoptnt.model.trade.cart.enums.CheckedWay;
import cn.shoptnt.model.trade.cart.vo.CartSkuOriginVo;
import cn.shoptnt.model.trade.cart.vo.CartSkuVO;
import cn.shoptnt.model.trade.cart.vo.CartView;
import cn.shoptnt.model.trade.cart.vo.PriceDetailVO;
import cn.shoptnt.service.trade.cart.CartOriginDataManager;
import cn.shoptnt.service.trade.cart.CartReadManager;
import cn.shoptnt.framework.exception.ServiceException;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;

/**
 * 购物车接口
 *
 * @author Snow
 * @version v1.0
 * 2018年03月19日21:40:52
 * @since v7.0.0
 */
@Tag(name = "购物车接口模块")
@RestController
@RequestMapping("/buyer/trade/carts")
@Validated
public class CartBuyerController {

    @Autowired
    private CartReadManager cartReadManager;


    @Autowired
    private CartOriginDataManager cartOriginDataManager;


    private final Logger logger = LoggerFactory.getLogger(getClass());


    @Operation(summary = "向购物车中添加一个产品")
    @Parameters({
            @Parameter(name = "sku_id", description = "产品ID", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "num", description = "此产品的购买数量", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "activity_id", description = "默认参与的活动id",  in = ParameterIn.QUERY),
            @Parameter(name = "promotion_type", description = "活动类型",   in = ParameterIn.QUERY)
    })
    @ResponseBody
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public CartSkuOriginVo add(@Parameter(hidden = true) @NotNull(message = "产品id不能为空") Long skuId,
                               @Parameter(hidden = true) @NotNull(message = "购买数量不能为空") @Min(value = 1, message = "加入购物车数量必须大于0") Integer num,
                               @Parameter(hidden = true) Long activityId,
                               @Parameter(hidden = true) String promotionType) {
        return cartOriginDataManager.add(skuId, num, activityId,promotionType);
    }


    @Operation(summary = "立即购买")
    @Parameters({
            @Parameter(name = "sku_id", description = "产品ID", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "num", description = "此产品的购买数量", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "activity_id", description = "默认参与的活动id",  in = ParameterIn.QUERY),
            @Parameter(name = "promotion_type", description = "活动类型",   in = ParameterIn.QUERY),
    })
    @ResponseBody
    @PostMapping("/buy")
    public void buy(@Parameter(hidden = true) @NotNull(message = "产品id不能为空") Long skuId,
                    @Parameter(hidden = true) @NotNull(message = "购买数量不能为空") @Min(value = 1, message = "购买数量必须大于0") Integer num,
                    @Parameter(hidden = true) Long activityId,
                    @Parameter(hidden = true) String promotionType) {
        cartOriginDataManager.buy(skuId,num,activityId,promotionType);
     }


    @Operation(summary = "获取购物车页面购物车详情")
    @Parameters({
            @Parameter(name = "way", description = "结算页面方式，BUY_NOW：立即购买，CART：购物车", required = true),
    })
    @GetMapping("/all")
    public CartView cartAll(String way) {

        try{

            return this.cartReadManager.getCartListAndCountPrice(CheckedWay.valueOf(way));

        }catch (Exception e){
            logger.error("读取购物车异常",e);
            return new CartView(new ArrayList<>(),new PriceDetailVO());
        }

    }


    @Operation(summary = "获取结算页面购物车详情")
    @Parameters({
            @Parameter(name = "way", description = "结算页面方式，BUY_NOW：立即购买，CART：购物车", required = true),
    })
    @GetMapping("/checked")
    public CartView cartChecked(String way) {

        try{

            // 读取选中的列表
           return  this.cartReadManager.getCheckedItems(CheckedWay.valueOf(way));

        }catch (Exception e){
            logger.error("读取结算页的购物车异常",e);
            return new CartView(new ArrayList<>(),new PriceDetailVO());
        }


    }


    @Operation(summary = "更新购物车中的多个产品")
    @Parameters({
            @Parameter(name = "sku_id", description = "产品id数组", required = true,  in = ParameterIn.PATH),
            @Parameter(name = "checked", description = "是否选中",  in = ParameterIn.QUERY),
            @Parameter(name = "num", description = "产品数量",  in = ParameterIn.QUERY),
    })
    @ResponseBody
    @PostMapping(value = "/sku/{sku_id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void update(@Parameter(hidden = true) @NotNull(message = "产品id不能为空") @PathVariable(name = "sku_id") Long skuId,
                         @Min(value = 0) @Max(value = 1) Integer checked, Integer num) {
        if (checked != null) {
            cartOriginDataManager.checked(skuId, checked);

        } else if (num != null) {
            cartOriginDataManager.updateNum(skuId, num);

        }
    }


    @Operation(summary = "设置全部商为选中或不选中")
    @Parameters({
            @Parameter(name = "checked", description = "是否选中", required = true,  in = ParameterIn.QUERY),
    })
    @ResponseBody
    @PostMapping(value = "/checked", produces = MediaType.APPLICATION_JSON_VALUE)
    public void updateAll(@NotNull(message = "必须指定是否选中") @Min(value = 0, message = "是否选中参数异常") @Max(value = 1, message = "是否选中参数异常") Integer checked) {
        if (checked != null) {
           cartOriginDataManager.checkedAll(checked, CheckedWay.CART);
        }

    }


    @Operation(summary = "批量设置某商家的商品为选中或不选中")
    @Parameters({
            @Parameter(name = "seller_id", description = "卖家id", required = true,  in = ParameterIn.PATH),
            @Parameter(name = "checked", description = "是否选中", required = true,  in = ParameterIn.QUERY),
    })
    @ResponseBody
    @PostMapping(value = "/seller/{seller_id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void updateSellerAll(@NotNull(message = "卖家id不能为空") @PathVariable(name = "seller_id") Long sellerId,
                                @NotNull(message = "必须指定是否选中") @Min(value = 0) @Max(value = 1) Integer checked) {
        if (checked != null && sellerId != null) {
            cartOriginDataManager.checkedSeller(sellerId,checked);
        }
    }


    @Operation(summary = "清空购物车")
    @DeleteMapping()
    public void clean() {
        cartOriginDataManager.clean();
    }


    @Operation(summary = "删除购物车中的一个或多个产品")
    @Parameters({
            @Parameter(name = "sku_ids", description = "产品id，多个产品可以用英文逗号：(,) 隔开", required = true,  in = ParameterIn.PATH),
    })
    @DeleteMapping(value = "/{sku_ids}/sku")
    public void delete(@PathVariable(name = "sku_ids") Long[] skuIds) {

        if (skuIds.length == 0) {
            throw new ServiceException(TradeErrorCode.E455.code(), "参数异常");
        }
        cartOriginDataManager.delete(skuIds,CheckedWay.CART);

    }

}
