/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.distribution;

import cn.shoptnt.model.errorcode.DistributionErrorCode;
import cn.shoptnt.service.distribution.exception.DistributionException;
import cn.shoptnt.model.distribution.vo.DistributionOrderVO;
import cn.shoptnt.model.distribution.vo.DistributionSellbackOrderVO;
import cn.shoptnt.service.distribution.DistributionOrderManager;
import cn.shoptnt.framework.database.WebPage;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 分销订单订单
 *
 * @author Chopper
 * @version v1.0
 * @Description:
 * @since v7.0
 * 2018/5/28 上午11:12
 */
@Tag(name = "分销订单")
@RestController
@RequestMapping("/admin/distribution/order")
public class DistributionOrderManagerController {
    @Autowired
    private DistributionOrderManager distributionOrderManager;

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Operation(summary = "结算单 分销订单查询")
    @GetMapping()
    @Parameters({
            @Parameter(name = "bill_id", description = "会员结算单id", in = ParameterIn.QUERY),
            @Parameter(name = "member_id", description = "会员id", in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "页码大小", in = ParameterIn.QUERY),
            @Parameter(name = "page_no", description = "页码", in = ParameterIn.QUERY),
    })
    public WebPage<DistributionOrderVO> billOrder(@Parameter(hidden = true) Long pageNo,@Parameter(hidden = true) Long pageSize, @Parameter(hidden = true) Long billId, @Parameter(hidden = true) Long memberId) throws Exception {
        try {
            return distributionOrderManager.pageDistributionOrder(pageSize, pageNo, memberId, billId);
        } catch (DistributionException e) {
            throw e;
        } catch (Exception e) {
            this.logger.error("查询结算单-》订单异常", e);
            throw new DistributionException(DistributionErrorCode.E1000.code(), DistributionErrorCode.E1000.des());
        }
    }


    @Operation(summary = "结算单 分销退款订单查询")
    @GetMapping("/sellback")
    @Parameters({
            @Parameter(name = "bill_id", description = "结算单id", in = ParameterIn.QUERY),
            @Parameter(name = "member_id", description = "会员id", in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "页码大小", in = ParameterIn.QUERY ),
            @Parameter(name = "page_no", description = "页码", in = ParameterIn.QUERY ),
    })
    public WebPage<DistributionSellbackOrderVO> billSellbackOrder(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize, @Parameter(hidden = true) Long billId,@Parameter(hidden = true) Long memberId) {
        try {
            return distributionOrderManager.pageSellBackOrder(pageSize, pageNo, memberId, billId);
        } catch (DistributionException e) {
            throw e;
        } catch (Exception e) {
            this.logger.error("查询结算单-》退款单异常", e);
            throw new DistributionException(DistributionErrorCode.E1000.code(), DistributionErrorCode.E1000.des());
        }
    }


}
