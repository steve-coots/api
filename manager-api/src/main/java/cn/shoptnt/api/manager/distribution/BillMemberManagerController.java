/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.distribution;

import cn.shoptnt.model.errorcode.DistributionErrorCode;
import cn.shoptnt.service.distribution.exception.DistributionException;
import cn.shoptnt.model.distribution.vo.BillMemberVO;
import cn.shoptnt.service.distribution.BillMemberManager;
import cn.shoptnt.framework.database.WebPage;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


import java.util.List;

/**
 * 分销会员结算单控制器
 *
 * @author Chopper
 * @version v1.0
 * @Description:
 * @since v7.0
 * 2018/5/24 下午2:39
 */
@Tag(name = "分销会员结算单控制器")
@RestController
@RequestMapping("/admin/distribution/bill/member")
public class BillMemberManagerController {

    @Autowired
    private BillMemberManager billMemberManager;

    @Operation(description = "分销商分页")
    @Parameters({
            @Parameter(name = "total_id", description = "总结算单id", in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "页码大小", in = ParameterIn.QUERY),
            @Parameter(name = "page_no", description = "页码", in = ParameterIn.QUERY),
            @Parameter(name = "uname", description = "会员名", in = ParameterIn.QUERY)
    })
    @GetMapping
    public WebPage<BillMemberVO> page(@Parameter(hidden = true) Long totalId, @Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize, String uname) {

        if (totalId == null) {
            throw new DistributionException(DistributionErrorCode.E1000.code(), DistributionErrorCode.E1000.des());
        }
        try {
            return billMemberManager.page(pageNo, pageSize, totalId, uname);
        } catch (Exception e) {
            throw new DistributionException(DistributionErrorCode.E1000.code(), DistributionErrorCode.E1000.des());
        }
    }

    @GetMapping("/{id}")
    @Operation(summary = "获取某个业绩详情")
    @Parameter(name = "id", description = "业绩单id", in = ParameterIn.QUERY)
    public BillMemberVO billMemberVO(@PathVariable Long id) {
        try {
            return new BillMemberVO(billMemberManager.getBillMember(id));
        } catch (Exception e) {

            throw new DistributionException(DistributionErrorCode.E1000.code(), DistributionErrorCode.E1000.des());
        }
    }


    @GetMapping("/down")
    @Operation(summary = "获取某个分销商下级业绩")
    @Parameters({
            @Parameter(name = "id", description = "当前页面 业绩单id", in = ParameterIn.QUERY),
            @Parameter(name = "member_id", description = "会员id", in = ParameterIn.QUERY)
    })
    public List<BillMemberVO> downBillMemberVO(Long id, @Parameter(hidden = true) Long memberId) {
        try {
            List<BillMemberVO> list = billMemberManager.allDown(memberId, id);
            return list;
        } catch (Exception e) {
            e.printStackTrace();
            throw new DistributionException(DistributionErrorCode.E1000.code(), DistributionErrorCode.E1000.des());
        }
    }

    @Operation(summary = "导出会员结算单")
    @Parameters({
            @Parameter(name = "total_id", description = "总结算单id", in = ParameterIn.QUERY),
    })
    @GetMapping("/export")
    public List<BillMemberVO> export(@Parameter(hidden = true) Long totalId) {
        return billMemberManager.page(1L, 99999L, totalId, "").getData();
    }

}
