/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.promotion;

import cn.shoptnt.model.promotion.exchange.dos.ExchangeCat;
import cn.shoptnt.model.promotion.exchange.dto.ExchangeQueryParam;
import cn.shoptnt.service.promotion.exchange.ExchangeGoodsManager;
import cn.shoptnt.framework.database.WebPage;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 积分商品控制器
 *
 * @author Snow create in 2018/5/29
 * @version v2.0
 * @since v7.0.0
 */
@RestController
@RequestMapping("/admin/promotion/exchange-goods")
@Tag(name = "积分商品相关API")
@Validated
public class ExchangeGoodsManagerController {

    @Autowired
    private ExchangeGoodsManager exchangeGoodsManager;


    @Operation(summary = "查询积分商品列表")
    @Parameters({
            @Parameter(name = "goods_name", description = "商品名称", in = ParameterIn.QUERY),
            @Parameter(name = "goods_sn", description = "商品编号", in = ParameterIn.QUERY),
            @Parameter(name = "seller_name", description = "店铺名称", in = ParameterIn.QUERY),
            @Parameter(name = "cat_id", description = "积分分类ID", in = ParameterIn.QUERY),
            @Parameter(name = "page_no", description = "取消原因", in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "取消原因", in = ParameterIn.QUERY)

    })
    @GetMapping
    public WebPage list(@Parameter(hidden = true) String goodsName, @Parameter(hidden = true) String goodsSn, @Parameter(hidden = true) String sellerName,
                        @Parameter(hidden = true) Long catId, @Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize) {

        ExchangeQueryParam param = new ExchangeQueryParam();
        param.setName(goodsName);
        param.setSn(goodsSn);
        param.setSellerName(sellerName);
        param.setCatId(catId);
        param.setPageNo(pageNo);
        param.setPageSize(pageSize);

        return this.exchangeGoodsManager.list(param);
    }

}
