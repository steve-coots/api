/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.statistics;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.base.SearchCriteria;
import cn.shoptnt.model.statistics.vo.MapChartData;
import cn.shoptnt.model.statistics.vo.MultipleChart;
import cn.shoptnt.model.statistics.vo.SalesTotal;
import cn.shoptnt.model.statistics.vo.SimpleChart;
import cn.shoptnt.service.statistics.OrderStatisticManager;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


/**
 * 订单统计
 *
 * @author chopper
 * @version v1.0
 * @Description:
 * @since v7.0
 * 2018/5/8 下午6:47
 */


@RestController
@Tag(name = "后台统计=》其他统计")
@RequestMapping("/admin/statistics/order")
public class OrderStatisticManagerController {

    @Autowired
    private OrderStatisticManager orderAnalysisManager;

    @Parameters({
            @Parameter(name = "cycle_type", description = "日期类型", in = ParameterIn.QUERY, required = true),
            @Parameter(name = "year", description = "年份", in = ParameterIn.QUERY, required = true),
            @Parameter(name = "month", description = "月份", in = ParameterIn.QUERY),
            @Parameter(name = "seller_id", description = "店铺id", in = ParameterIn.QUERY),
            @Parameter(name = "order_status", description = "订单状态", in = ParameterIn.QUERY)
    })
    @Operation(summary = "其他统计=》订单统计=》下单金额")
    @GetMapping(value = "/order/money")
    public MultipleChart getOrderMoney(@Parameter(hidden = true) SearchCriteria searchCriteria, @Parameter(hidden = true) String orderStatus) {
        return this.orderAnalysisManager.getOrderMoney(searchCriteria, orderStatus);
    }

    @Parameters({
            @Parameter(name = "cycle_type", description = "日期类型", in = ParameterIn.QUERY, required = true),
            @Parameter(name = "year", description = "年份", in = ParameterIn.QUERY, required = true),
            @Parameter(name = "month", description = "月份", in = ParameterIn.QUERY),
            @Parameter(name = "seller_id", description = "店铺id", in = ParameterIn.QUERY),
            @Parameter(name = "order_status", description = "订单状态", in = ParameterIn.QUERY)
    })
    @Operation(summary = "其他统计=》订单统计=》下单数量")
    @GetMapping(value = "/order/num")
    public MultipleChart getOrderNum(@Parameter(hidden = true) SearchCriteria searchCriteria, @Parameter(hidden = true) String orderStatus) {
        return this.orderAnalysisManager.getOrderNum(searchCriteria, orderStatus);
    }

    @Parameters({
            @Parameter(name = "cycle_type", description = "日期类型", in = ParameterIn.QUERY, required = true),
            @Parameter(name = "year", description = "年份", in = ParameterIn.QUERY, required = true),
            @Parameter(name = "month", description = "月份", in = ParameterIn.QUERY),
            @Parameter(name = "seller_id", description = "店铺id", in = ParameterIn.QUERY),
            @Parameter(name = "order_status", description = "订单状态", in = ParameterIn.QUERY),
            @Parameter(name = "page_no", description = "页码", in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "分页大小", in = ParameterIn.QUERY)
    })
    @Operation(summary = "其他统计=》订单统计=》下单数量")
    @GetMapping(value = "/order/page")
    public WebPage getOrderPage(@Parameter(hidden = true) SearchCriteria searchCriteria, @Parameter(hidden = true) String orderStatus, @Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize) {
        return this.orderAnalysisManager.getOrderPage(searchCriteria, orderStatus, pageNo, pageSize);
    }

    @Parameters({
            @Parameter(name = "cycle_type", description = "日期类型", in = ParameterIn.QUERY, required = true),
            @Parameter(name = "year", description ="年份", in = ParameterIn.QUERY, required = true),
            @Parameter(name = "month", description = "月份", in = ParameterIn.QUERY),
            @Parameter(name = "seller_id", description = "店铺id", in = ParameterIn.QUERY)
    })
    @Operation(summary = "其他统计=》销售收入统计 page")
    @GetMapping(value = "/sales/money")
    public WebPage getSalesMoney(@Parameter(hidden = true) SearchCriteria searchCriteria, @Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize) {
        return this.orderAnalysisManager.getSalesMoney(searchCriteria, pageNo, pageSize);
    }


    @Operation(summary = "其他统计=》销售收入 退款统计 page")
    @Parameters({
            @Parameter(name = "cycle_type", description = "日期类型", in = ParameterIn.QUERY, required = true),
            @Parameter(name = "year", description = "年份", in = ParameterIn.QUERY, required = true),
            @Parameter(name = "month", description = "月份", in = ParameterIn.QUERY),
            @Parameter(name = "seller_id", description = "店铺id", in = ParameterIn.QUERY)
    })
    @GetMapping(value = "/aftersales/money")
    public WebPage getAfterSalesMoney(@Parameter(hidden = true) SearchCriteria searchCriteria, @Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize) {
        return this.orderAnalysisManager.getAfterSalesMoney(searchCriteria, pageNo, pageSize);
    }

    @Parameters({
            @Parameter(name = "cycle_type", description = "日期类型", in = ParameterIn.QUERY, required = true),
            @Parameter(name = "year", description = "年份", in = ParameterIn.QUERY, required = true),
            @Parameter(name = "month", description = "月份", in = ParameterIn.QUERY),
            @Parameter(name = "seller_id", description = "店铺id", in = ParameterIn.QUERY)
    })
    @Operation(summary = "其他统计=》销售收入总览")
    @GetMapping(value = "/sales/total")
    public SalesTotal getSalesMoneyTotal(@Parameter(hidden = true) SearchCriteria searchCriteria) {
        return this.orderAnalysisManager.getSalesMoneyTotal(searchCriteria);
    }

    @Parameters({
            @Parameter(name = "cycle_type", description = "日期类型", in = ParameterIn.QUERY, required = true),
            @Parameter(name = "year", description = "年份", in = ParameterIn.QUERY, required = true),
            @Parameter(name = "month", description = "月份", in = ParameterIn.QUERY),
            @Parameter(name = "seller_id", description = "店铺id", in = ParameterIn.QUERY)
    })
    @Operation(summary = "区域分析=>下单会员数")
    @GetMapping(value = "/region/member")
    public MapChartData getOrderRegionMember(@Parameter(hidden = true) SearchCriteria searchCriteria) {
        return this.orderAnalysisManager.getOrderRegionMember(searchCriteria);
    }

    @Parameters({
            @Parameter(name = "cycle_type", description = "日期类型", in = ParameterIn.QUERY, required = true),
            @Parameter(name = "year", description = "年份", in = ParameterIn.QUERY, required = true),
            @Parameter(name = "month", description = "月份", in = ParameterIn.QUERY),
            @Parameter(name = "seller_id", description = "店铺id", in = ParameterIn.QUERY)
    })
    @Operation(summary = "区域分析=>下单量")
    @GetMapping(value = "/region/num")
    public MapChartData getOrderRegionNum(@Parameter(hidden = true) SearchCriteria searchCriteria) {
        return this.orderAnalysisManager.getOrderRegionNum(searchCriteria);
    }

    @Parameters({
            @Parameter(name = "cycle_type", description = "日期类型", in = ParameterIn.QUERY, required = true),
            @Parameter(name = "year", description = "年份", in = ParameterIn.QUERY, required = true),
            @Parameter(name = "month", description = "月份", in = ParameterIn.QUERY),
            @Parameter(name = "seller_id", description = "店铺id", in = ParameterIn.QUERY)
    })
    @Operation(summary = "区域分析=>下单金额")
    @GetMapping(value = "/region/money")
    public MapChartData getOrderRegionMoney(@Parameter(hidden = true) SearchCriteria searchCriteria) {
        return this.orderAnalysisManager.getOrderRegionMoney(searchCriteria);
    }

    @Parameters({
            @Parameter(name = "cycle_type", description = "日期类型", in = ParameterIn.QUERY, required = true),
            @Parameter(name = "year", description = "年份", in = ParameterIn.QUERY, required = true),
            @Parameter(name = "month", description = "月份", in = ParameterIn.QUERY),
            @Parameter(name = "seller_id", description = "店铺id", in = ParameterIn.QUERY)
    })
    @Operation(summary = "区域分析表格=>page")
    @GetMapping(value = "/region/form")
    public WebPage getOrderRegionForm(@Parameter(hidden = true) SearchCriteria searchCriteria) {
        return this.orderAnalysisManager.getOrderRegionForm(searchCriteria);
    }

    @Parameters({
            @Parameter(name = "cycle_type", description = "日期类型", in = ParameterIn.QUERY, required = true),
            @Parameter(name = "year", description = "年份", in = ParameterIn.QUERY, required = true),
            @Parameter(name = "month", description = "月份", in = ParameterIn.QUERY),
            @Parameter(name = "seller_id", description = "店铺id", in = ParameterIn.QUERY),
            @Parameter(name = "prices", description = "价格区间", in = ParameterIn.QUERY)
    })
    @Operation(summary = "客单价分布=>客单价分布")
    @GetMapping(value = "/unit/price")
    public SimpleChart getUnitPrice(@Parameter(hidden = true) SearchCriteria searchCriteria, @RequestParam(required = false) Integer[] prices) {
        return this.orderAnalysisManager.getUnitPrice(searchCriteria, prices);
    }

    @Operation(summary = "客单价分布=>购买频次分析")
    @GetMapping(value = "/unit/num")
    public WebPage getUnitNum() {
        return this.orderAnalysisManager.getUnitNum();
    }

    @Parameters({
            @Parameter(name = "cycle_type", description = "日期类型", in = ParameterIn.QUERY, required = true),
            @Parameter(name = "year", description = "年份", in = ParameterIn.QUERY, required = true),
            @Parameter(name = "month", description = "月份", in = ParameterIn.QUERY),
            @Parameter(name = "seller_id", description = "店铺id", in = ParameterIn.QUERY),
            @Parameter(name = "prices", description = "价格区间", in = ParameterIn.QUERY)
    })
    @Operation(summary = "客单价分布=>购买时段分析")
    @GetMapping(value = "/unit/time")
    public SimpleChart getUnitTime(@Parameter(hidden = true) SearchCriteria searchCriteria) {
        return this.orderAnalysisManager.getUnitTime(searchCriteria);
    }

    @Parameters({
            @Parameter(name = "cycle_type", description = "日期类型", in = ParameterIn.QUERY, required = true),
            @Parameter(name = "year", description = "年份", in = ParameterIn.QUERY, required = true),
            @Parameter(name = "month", description = "月份", in = ParameterIn.QUERY),
            @Parameter(name = "seller_id", description = "店铺id", in = ParameterIn.QUERY)
    })
    @Operation(summary = "退款统计")
    @GetMapping(value = "/return/money")
    public SimpleChart getReturnMoney(@Parameter(hidden = true) SearchCriteria searchCriteria) {
        return this.orderAnalysisManager.getReturnMoney(searchCriteria);
    }

}
