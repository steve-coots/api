/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.member;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.service.member.MemberCouponManager;
import cn.shoptnt.model.promotion.coupon.dos.CouponDO;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


/**
 * 会员优惠券
 *
 * @author Snow create in 2018/6/13
 * @version v2.0
 * @since v7.0.0
 */
@RestController
@RequestMapping("/admin/members/coupon")
@Tag(name = "会员优惠券相关API")
@Validated
public class MemberCouponManagerController {

    @Autowired
    private MemberCouponManager memberCouponManager;


    @Operation(summary = "查询某优惠券领取列表")
    @Parameters({
            @Parameter(name = "page_no", description = "页码", in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量", in = ParameterIn.QUERY),
            @Parameter(name = "coupon_id", description = "开始时间", in = ParameterIn.QUERY),
    })
    @GetMapping
    public WebPage list(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize,
                        @Parameter(hidden = true) Long couponId) {

        return this.memberCouponManager.queryByCouponId(couponId, pageNo, pageSize);
    }


    @Operation(summary = "废弃某优惠券")
    @Parameters({
            @Parameter(name = "page_no", description = "页码", in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量", in = ParameterIn.QUERY),
            @Parameter(name = "coupon_id", description = "开始时间", in = ParameterIn.QUERY),
    })
    @PutMapping("/{member_coupon_id}/cancel")
    public String list(@Parameter(hidden = true) @PathVariable("member_coupon_id") Long memberCouponId) {

        this.memberCouponManager.cancel(memberCouponId);

        return "";
    }


}
