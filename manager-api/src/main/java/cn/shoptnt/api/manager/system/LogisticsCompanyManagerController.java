/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.system;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.system.dto.FormItem;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;




import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import cn.shoptnt.model.system.dos.LogisticsCompanyDO;
import cn.shoptnt.service.system.LogisticsCompanyManager;
import java.util.List;

/**
 * 物流公司控制器
 *
 * @author zjp
 * @version v7.0.0
 * @since v7.0.0
 * 2018-03-29 15:10:38
 */
@RestController
@RequestMapping("/admin/systems/logi-companies")
@Tag(name = "物流公司相关API")
@Validated
public class LogisticsCompanyManagerController {

    @Autowired
    private LogisticsCompanyManager logisticsCompanyManager;


    @Operation(summary = "查询物流公司列表")
    @Parameters({
            @Parameter(name = "page_no", description = "页码", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "name", description = "物流公司名称",   in = ParameterIn.QUERY)
    })
    @GetMapping
    public WebPage list(@Parameter(hidden = true) @NotNull(message = "页码不能为空") Long pageNo, @Parameter(hidden = true) @NotNull(message = "每页数量不能为空") Long pageSize, @Parameter(hidden = true) String name) {

        return this.logisticsCompanyManager.list(pageNo, pageSize, name);
    }


    @Operation(summary = "添加物流公司")
    @PostMapping
    public LogisticsCompanyDO add(@Valid LogisticsCompanyDO logi,@RequestBody @Valid List<FormItem> formItems) {

        logi.setForm(formItems);
        this.logisticsCompanyManager.add(logi);

        return logi;
    }

    @PutMapping(value = "/{id}")
    @Operation(summary = "修改物流公司")
    @Parameters({
            @Parameter(name = "id", description = "主键", required = true,  in = ParameterIn.PATH)
    })
    public LogisticsCompanyDO edit(@Valid LogisticsCompanyDO logi, @PathVariable("id") Long id,@RequestBody @Valid List<FormItem> formItems) {

        logi.setForm(formItems);
        this.logisticsCompanyManager.edit(logi, id);

        return logi;
    }


    @DeleteMapping(value = "/{id}")
    @Operation(summary = "删除物流公司")
    @Parameters({
            @Parameter(name = "id", description = "要删除的物流公司主键", required = true,  in = ParameterIn.PATH)
    })
    public String delete(@PathVariable("id") Long id) {
        this.logisticsCompanyManager.delete(id);

        return "";
    }


    @GetMapping(value = "/{id}")
    @Operation(summary = "查询一个物流公司")
    @Parameters({
            @Parameter(name = "id", description = "要查询的物流公司主键", required = true,  in = ParameterIn.PATH)
    })
    public LogisticsCompanyDO get(@PathVariable("id") Long id) {
        LogisticsCompanyDO logi = this.logisticsCompanyManager.getModel(id);
        return logi;
    }

    @PostMapping(value = "/{id}")
    @Operation(summary = "开启或禁用物流公司")
    @Parameters({
            @Parameter(name = "id", description = "物流公司主键ID", required = true,  in = ParameterIn.PATH),
            @Parameter(name = "disabled", description = "状态 OPEN：开启，CLOSE：禁用", required = true,   in = ParameterIn.PATH)
    })
    public String openClose(@PathVariable("id") Long id, @Parameter(hidden = true) String disabled) {
        this.logisticsCompanyManager.openCloseLogi(id, disabled);

        return "";
    }

}
