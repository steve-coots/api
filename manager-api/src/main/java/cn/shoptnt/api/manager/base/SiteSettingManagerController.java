/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.base;

import cn.shoptnt.handler.AdminTwoStepAuthentication;
import cn.shoptnt.model.base.SettingGroup;
import cn.shoptnt.model.support.LogClient;
import cn.shoptnt.model.support.validator.annotation.Log;
import cn.shoptnt.model.support.validator.annotation.LogLevel;
import cn.shoptnt.service.base.service.SettingManager;
import cn.shoptnt.model.system.vo.SiteSetting;
import cn.shoptnt.framework.util.JsonUtil;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * 站点设置api
 *
 * @author zh
 * @version v7.0
 * @date 18/5/18 下午6:55
 * @since v7.0
 */
@RestController
@RequestMapping("/admin/settings")
@Tag(name = "站点设置")
@Validated
public class SiteSettingManagerController {
    @Autowired
    private SettingManager settingManager;

    @Autowired
    private AdminTwoStepAuthentication adminTwoStepAuthentication;


    @GetMapping(value = "/site")
    @Operation(summary = "获取站点设置")
    public SiteSetting getSiteSetting() {
        String siteSettingJson = settingManager.get(SettingGroup.SITE);

        SiteSetting siteSetting = JsonUtil.jsonToObject(siteSettingJson,SiteSetting.class);
        if (siteSetting == null) {
            return new SiteSetting();
        }
        return siteSetting;
    }

    @PutMapping(value = "/site")
    @Operation(summary = "修改站点设置")
    @Log(client = LogClient.admin,detail = "修改系统参数",level = LogLevel.important)
    public SiteSetting editSiteSetting(@Valid SiteSetting siteSetting) {
        //调用二次验证
        this.adminTwoStepAuthentication.sensitive();

        settingManager.save(SettingGroup.SITE, siteSetting);
        return siteSetting;
    }

}
