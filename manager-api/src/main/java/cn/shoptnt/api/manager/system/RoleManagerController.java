/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.system;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.system.dos.RoleDO;
import cn.shoptnt.model.system.vo.RoleVO;
import cn.shoptnt.service.system.RoleManager;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


import javax.validation.constraints.NotEmpty;
import java.util.List;


/**
 * 角色表控制器
 *
 * @author admin
 * @version v1.0.0
 * @since v7.0.0
 * 2018-04-17 16:48:27
 */
@RestController
@RequestMapping("/admin/systems/roles")
@Tag(name = "角色表相关API")
public class RoleManagerController {

    @Autowired
    private RoleManager roleManager;


    @Operation(summary = "查询角色列表")
    @Parameters({
            @Parameter(name = "page_no", description = "页码", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "keyword", description = "关键字",   in = ParameterIn.QUERY)
    })
    @GetMapping
    public WebPage list(@Parameter(hidden = true) @NotEmpty(message = "页码不能为空") Long pageNo, @Parameter(hidden = true) @NotEmpty(message = "每页数量不能为空") Long pageSize, @Parameter(hidden = true) String keyword) {
        return this.roleManager.list(pageNo, pageSize, keyword);
    }


    @Operation(summary = "添加角色")
    @Parameters({
            @Parameter(name = "roleVO", description = "角色", required = true)
    })
    @PostMapping
    public RoleVO add(@RequestBody @Parameter(hidden = true) RoleVO roleVO) {
        return this.roleManager.add(roleVO);
    }

    @PutMapping(value = "/{id}")
    @Operation(summary = "修改角色表")
    @Parameters({
            @Parameter(name = "id", description = "主键", required = true,  in = ParameterIn.PATH),
            @Parameter(name = "roleVO", description = "菜单", required = true)
    })
    public RoleVO edit(@RequestBody @Parameter(hidden = true) RoleVO roleVO, @PathVariable Long id) {
        return this.roleManager.edit(roleVO, id);
    }


    @DeleteMapping(value = "/{id}")
    @Operation(summary = "删除角色")
    @Parameters({
            @Parameter(name = "id", description = "要删除的角色表主键", required = true,  in = ParameterIn.PATH)
    })
    public String delete(@PathVariable Long id) {
        this.roleManager.delete(id);
        return "";
    }


    @GetMapping(value = "/{id}")
    @Operation(summary = "查询一个角色表")
    @Parameters({
            @Parameter(name = "id", description = "要查询的角色表主键", required = true,  in = ParameterIn.PATH)
    })
    public RoleVO get(@PathVariable Long id) {
        return this.roleManager.getRole(id);
    }

    @GetMapping(value = "/{id}/checked")
    @Operation(summary = "根据角色id查询所拥有的菜单权限")
    @Parameters({
            @Parameter(name = "id", description = "要查询的角色表主键", required = true,  in = ParameterIn.PATH)
    })
    public List<String> getCheckedMenu(@PathVariable Long id) {
        return this.roleManager.getRoleMenu(id);
    }

}
