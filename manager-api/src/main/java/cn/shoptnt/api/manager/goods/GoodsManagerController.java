/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.goods;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.goods.dto.GoodsAuditParam;
import cn.shoptnt.model.goods.dto.GoodsQueryParam;
import cn.shoptnt.model.goods.enums.Permission;
import cn.shoptnt.model.goods.vo.GoodsSelectLine;
import cn.shoptnt.model.goods.vo.GoodsSkuVO;
import cn.shoptnt.model.support.LogClient;
import cn.shoptnt.model.support.validator.annotation.Log;
import cn.shoptnt.service.goods.GoodsManager;
import cn.shoptnt.service.goods.GoodsQueryManager;
import cn.shoptnt.service.goods.GoodsSkuManager;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.List;

/**
 * 商品控制器
 *
 * @author fk
 * @version v2.0
 * @since v7.0.0 2018-03-21 11:23:10
 */
@RestController
@RequestMapping("/admin/goods")
@Tag(name = "商品相关API")
@Validated
public class GoodsManagerController {

    @Autowired
    private GoodsManager goodsManager;
    @Autowired
    private GoodsQueryManager goodsQueryManager;
    @Autowired
    private GoodsSkuManager goodsSkuManager;

    @Operation(summary = "查询商品或者审核列表")
    @GetMapping
    public WebPage list(GoodsQueryParam param, @Parameter(hidden = true) Long pageSize, @Parameter(hidden = true) Long pageNo) {

        param.setPageNo(pageNo);
        param.setPageSize(pageSize);
        return this.goodsQueryManager.list(param);
    }

    @Operation(summary = "查询SKU列表")
    @GetMapping("/skus")
    public WebPage skus(GoodsQueryParam param, @Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize) {
        param.setPageNo(pageNo);
        param.setPageSize(pageSize);
        return this.goodsSkuManager.list(param);
    }


    @Operation(summary = "管理员下架商品")
    @Parameters({
            @Parameter(name = "goods_id", description = "商品ID", required = true, in = ParameterIn.PATH),
            @Parameter(name = "reason", description = "下架理由", required = true, in = ParameterIn.QUERY)
    })
    @PutMapping(value = "/{goods_id}/under")
    @Log(client = LogClient.admin, detail = "商品下架，商品id :${goodsId}")
    public String underGoods(@PathVariable("goods_id") Long goodsId, @NotEmpty(message = "下架原因不能为空") String reason) {

        this.goodsManager.under(new Long[]{goodsId}, reason, Permission.ADMIN);

        return null;
    }

    @Operation(summary = "管理员上架商品")
    @Parameters({
            @Parameter(name = "goods_id", description = "商品ID", required = true, in = ParameterIn.PATH),
    })
    @PutMapping(value = "/{goods_id}/up")
    @Log(client = LogClient.admin, detail = "商品上架，商品id :${goodsId}")
    public String unpGoods(@PathVariable("goods_id") Long goodsId) {

        this.goodsManager.up(goodsId);

        return null;
    }


    @Operation(summary = "管理员批量审核商品")
    @PostMapping(value = "/batch/audit")
    public String batchAudit(@Valid @RequestBody GoodsAuditParam param) {

        this.goodsManager.batchAuditGoods(param);

        return null;
    }

    @GetMapping(value = "/{goods_ids}/details")
    @Operation(summary = "查询多个商品的基本信息")
    @Parameters({
            @Parameter(name = "goods_ids", description = "要查询的商品主键", required = true, in = ParameterIn.PATH),
            @Parameter(name = "param", description = "查询条件参数", in = ParameterIn.QUERY)
    })
    public List<GoodsSelectLine> getGoodsDetail(@PathVariable("goods_ids") Long[] goodsIds, GoodsQueryParam param) {

        return this.goodsQueryManager.queryGoodsLines(goodsIds, param);
    }

    @GetMapping(value = "/skus/{sku_ids}/details")
    @Operation(summary = "查询多个商品的基本信息")
    @Parameters({
            @Parameter(name = "sku_ids", description = "要查询的SKU主键", required = true, in = ParameterIn.PATH)})
    public List<GoodsSkuVO> getGoodsSkuDetail(@PathVariable("sku_ids") Long[] skuIds) {

        return this.goodsSkuManager.query(skuIds);
    }

}
