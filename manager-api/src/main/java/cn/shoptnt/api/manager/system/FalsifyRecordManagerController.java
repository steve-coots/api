/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.system;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.security.FalsifyRecordParams;
import cn.shoptnt.service.security.FalsifyRecordManager;
import cn.shoptnt.service.security.SignScanManager;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;



/**
 * 篡改记录控制器
 *
 * @author shenyanwu
 * @version v7.0
 * @since v7.0.0
 * 2021-11-21 20:38:26
 */
@RestController
@RequestMapping("/admin/systems/falsify-record")
@Tag(name = "篡改记录API")
@Validated
public class FalsifyRecordManagerController {

    @Autowired
    private FalsifyRecordManager falsifyRecordManager;
    @Autowired
    private SignScanManager signScanManager;

    @Operation(summary = "查询篡改记录列表")
    @Parameters({
            @Parameter(name = "page_no", description = "页码", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量", required = true,  in = ParameterIn.QUERY)
    })
    @GetMapping
    public WebPage list(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize, FalsifyRecordParams params) {
        params.setPageNo(pageNo);
        params.setPageSize(pageSize);
        return this.falsifyRecordManager.list(params);
    }


    @Operation(summary = "修复数据")
    @Parameters({
            @Parameter(name = "record_id", description = "篡改记录id", required = true,  in = ParameterIn.QUERY)
    })
    @PutMapping("/{record_id}")
    public void repair(@Parameter(hidden = true) @PathVariable("record_id") Long recordId) {
        this.falsifyRecordManager.repair(recordId);
    }

    @Operation(summary = "手动触发篡改数据扫描")
    @GetMapping("/scan")
    public void scan() {
        signScanManager.scan();
    }

}
