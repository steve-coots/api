/*
 * SHOPTNT 版权所有。
 * 未经许可，您不得使用此文件。
 * 官方地址：www.shoptnt.cn
 */
package cn.shoptnt.api.manager.system;

import cn.shoptnt.framework.database.WebPage;
import cn.shoptnt.model.system.dos.ExpressPlatformDO;
import cn.shoptnt.model.system.dos.SmsPlatformDO;
import cn.shoptnt.model.system.vo.ExpressDetailVO;
import cn.shoptnt.model.system.vo.ExpressPlatformVO;
import cn.shoptnt.service.system.ExpressPlatformManager;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.Parameters;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.tags.Tag;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


/**
 * 快递平台控制器
 *
 * @author zh
 * @version v7.0.0
 * @since v7.0.0
 * 2018-07-11 14:42:50
 */
@RestController
@RequestMapping("/admin/systems/express-platforms")
@Tag(name = "快递平台相关API")
public class ExpressPlatformManagerController {

    @Autowired
    private ExpressPlatformManager expressPlatformManager;


    @Operation(summary = "查询快递平台列表")
    @Parameters({
            @Parameter(name = "page_no", description = "页码", required = true,  in = ParameterIn.QUERY),
            @Parameter(name = "page_size", description = "每页显示数量", required = true,  in = ParameterIn.QUERY)
    })
    @GetMapping
    public WebPage list(@Parameter(hidden = true) Long pageNo, @Parameter(hidden = true) Long pageSize) {

        return this.expressPlatformManager.list(pageNo, pageSize);
    }

    @Operation(summary = "修改快递平台")
    @PutMapping(value = "/{bean}")
    @Parameters({
            @Parameter(name = "bean", description = "快递平台bean id", required = true,   in = ParameterIn.PATH),
            @Parameter(name = "express_platform", description = "快递平台对象", required = true)
    })
    public ExpressPlatformVO edit(@PathVariable String bean, @RequestBody @Parameter(hidden = true) ExpressPlatformVO expressPlatformVO) {
        expressPlatformVO.setBean(bean);
        return this.expressPlatformManager.edit(expressPlatformVO);
    }

    @Operation(summary = "获取快递平台的配置")
    @GetMapping("/{bean}")
    @Parameter(name = "bean", description = "快递平台bean id", required = true,   in = ParameterIn.PATH)
    public ExpressPlatformVO getUploadSetting(@PathVariable String bean) {
        return this.expressPlatformManager.getExoressConfig(bean);
    }

    @Operation(summary = "开启某个快递平台方案")
    @PutMapping("/{bean}/open")
    @Parameter(name = "bean", description = "bean", required = true,   in = ParameterIn.PATH)
    public String open(@PathVariable String bean) {
        this.expressPlatformManager.open(bean);
        return null;
    }

    @Operation(summary = "查询物流详细")
    @Parameters({
            @Parameter(name = "id", description = "物流公司id",  in = ParameterIn.QUERY),
            @Parameter(name = "num", description = "快递单号",   in = ParameterIn.QUERY),
    })
    @GetMapping("/express")
    public ExpressDetailVO list(@Parameter(hidden = true) Long id, @Parameter(hidden = true) String num) {
        return this.expressPlatformManager.getExpressDetail(id, num);
    }

}
